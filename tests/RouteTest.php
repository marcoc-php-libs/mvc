<?php
namespace Tests;

use mrblue\mvc\Exception\RouteNotMatchException;
use mrblue\mvc\Mvc;
use mrblue\mvc\Route;
use mrblue\mvc\RouteMatch;

class RouteTest extends \PHPUnit\Framework\TestCase
{

	/**
	 * 
	 * @dataProvider route1Ok
	 * @depends testGetRoute1
	 */
	function testRoute1Ok( $path , $route_name , $params , Route $Route )
	{
		$RouteMatch = $Route->match($path,'GET','');

		$this->assertInstanceOf(RouteMatch::class , $RouteMatch);
		
		$this->assertEquals($route_name , $RouteMatch->getRoute()->name);
		$this->assertEquals( 'GET', $RouteMatch->getMethod() );
		$this->assertEquals( $path, $RouteMatch->getPath() );
		$this->assertEquals( $params, $RouteMatch->getParams() );
		$this->assertEquals( $Route->getController(), $RouteMatch->getController() );
		$this->assertEquals( $RouteMatch->getRoute()->getViewTemplate() , $RouteMatch->getViewTemplate() );

	}

	/**
	 * 
	 * @dataProvider route1Ok
	 * @depends testGetRoute1
	 * @depends testGetRoute2
	 */
	function testRoute1OkUrl( $path , $route_name , $params , Route $Route , Route $Route2 )
	{
		$Mvc = new Mvc([
			'router' => [
				'routes' => [
					$Route2,
					$Route
				]
			]
		]);
		
		$this->assertEquals($path , $Mvc->url($path));
	}
		
	function route1Ok()
	{
		return [
			['/api/me','me',['action' => 'me']],
			['/api/survey/4','survey',['id'=>'4','action'=>'survey']],
			['/api/survey/57','survey',['id'=>'57','action'=>'survey']]
		];
	}

	/**
	 * 
	 * @dataProvider route1Err
	 * @depends testGetRoute1
	 */
	function testRoute1Err( $path , Route $Route )
	{
		$this->expectException(RouteNotMatchException::class);

		$Route->match($path,'GET','');
	}

	/**
	 * 
	 * @dataProvider route1Err
	 * @depends testGetRoute1
	 */
	function testRoute1ErrUrl( $path , Route $Route )
	{
		$this->expectException(RouteNotMatchException::class);

		$Mvc = new Mvc([
			'router' => [
				'routes' => [
					$Route
				]
			]
		]);

		$Mvc->url($path);
	}
		
	function route1Err()
	{
		return [
			['not'],
			['/api'],
			['/api/men'],
			['/api/m'],
			['/api/me/'],
			['//api/me'],
			['/api/me/2'],
			['/api/survey'],
			['/api/survey/'],
			['/api/survey/0a'],
			['/api/survey/1/'],
			['/api/survey/1/fail']
		];
	}

	/**
	 * 
	 * @dataProvider route2Ok
	 * @depends testGetRoute2
	 */
	function testRoute2Ok( $path , $method , $host , $params , Route $Route )
	{
		$RouteMatch = $Route->match($path,$method,$host);

		$this->assertInstanceOf(RouteMatch::class , $RouteMatch);
		
		$this->assertEquals( strtoupper($method), $RouteMatch->getMethod() );
		$this->assertEquals( $path, $RouteMatch->getPath() );
		$this->assertEquals( $host, $RouteMatch->getHostname() );
		$this->assertEquals( $params, $RouteMatch->getParams() );
		$this->assertEquals( 'ApiController', $RouteMatch->getController() );
		$this->assertEquals( $RouteMatch->getRoute()->getViewTemplate(), $RouteMatch->getViewTemplate() );

	}

	/**
	 * 
	 * @dataProvider route2Ok
	 * @depends testGetRoute2
	 * @depends testGetRoute1
	 */
	function testRoute2OkUrl( $path , $method , $host , $params , Route $Route , Route $Route1 )
	{
		$Mvc = new Mvc([
			'router' => [
				'routes' => [
					$Route1,
					$Route
				]
			]
		]);
		
		$url = $Mvc->fullUrl('http',$host,$path);
		$this->assertEquals("http://$host$path" , $url);
	}
		
	function route2Ok()
	{
		return [
			['/api','get','umarco.mrblue.com',['action' => 'homepage','user'=>'umarco']],
			['/api','get','test.umarco.mrblue.com',['action' => 'homepage','user'=>'umarco']],
			['/api','get','getme.umarco.mrblue.com',['action' => 'getme','user'=>'umarco']],
			['/api','get','test.getme.umarco.mrblue.com',['action' => 'getme','user'=>'umarco']],
			['/api/postdata','post','umarco.mrblue.com',['action' => 'postdata','user'=>'umarco']],
			['/api/postdata','post','overrided.umarco.mrblue.com',['action' => 'postdata','user'=>'umarco']],
			['/api/post-photo','post','umarco.chiodo.mrblue.com',['action' => 'post-photo','user'=>'umarco','subuser'=>'chiodo']],
		];
	}

	/**
	 * 
	 * @dataProvider route2Err
	 * @depends testGetRoute2
	 */
	function testRoute2err( $path , $method , $host , Route $Route )
	{
		$this->expectException(RouteNotMatchException::class);

		$Route->match($path,$method,$host);
	}

	/**
	 * 
	 * @dataProvider route2Err
	 * @depends testGetRoute2
	 */
	function testRoute2errUrl( $path , $method , $host , Route $Route )
	{
		$this->expectException(RouteNotMatchException::class);

		$Mvc = new Mvc([
			'router' => [
				'routes' => [
					$Route
				]
			]
		]);

		$Mvc->fullUrl('http',$host,$path);

		if( $method !== 'get'){
			throw new RouteNotMatchException();
		}
	}
		
	function route2Err()
	{
		return [
			['/api/','get','umarco.mrblue.com'],
			['/api/getme/','get','umarco.mrblue.com'],
			['/api/getme/ab','get','umarco.mrblue.com'],
			['/api','put','umarco.mrblue.com'],
			['/api','get','mrblue.com'],
			['/api','get','marco.mrblue.com'],
			['/api','get','umarco.mrblue.it'],
			['/api','get','umarco.mrgray.com'],
		];
	}


	function testGetRoute1()
	{
		$Route = new \mrblue\mvc\Route('root',[
			'type' => \mrblue\mvc\Route::TYPE_LITERAL,
			'equal_to' => '/api',
			'controller' => 'ApiController',
			'view_template' => __DIR__ . '/test_view.php',
			'may_terminate' => false,
			'childs' => [
				'me' => [
					'type' => \mrblue\mvc\Route::TYPE_LITERAL,
					'equal_to' => '/me',
					'defaults' => [
						'action' => 'me'
					],
					'view_template' => __DIR__ . '/test_view.php',
					'priority' => 10
				],
				'survey' => [
					'type' => \mrblue\mvc\Route::TYPE_SEGMENT,
					'equal_to' => '/survey/:id',
					'defaults' => [
						'action' => 'survey'
					],
					'constraints' => [
						'id' => '\d+'
					],
					'priority' => 8
				],
			]
		]);

		$this->assertEquals('root',$Route->name);
		$this->assertEquals('ApiController',$Route->getController());
		$this->assertEquals( __DIR__ . '/test_view.php',$Route->getViewTemplate());
		$this->assertEquals([],$Route->getDefaults());

		$RouteMe = $Route->getChild('me');
		$RouteSurvey = $Route->getChild('survey');

		$this->assertNull( $RouteMe->getController() );

		$this->assertEquals($Route->getController() , $RouteMe->getController(true));
		$this->assertEquals($Route->getViewTemplate() , $RouteMe->getViewTemplate(true));


		$this->assertNull( $RouteSurvey->getController() );
		$this->assertNull( $RouteSurvey->getViewTemplate() );

		$this->assertEquals($Route->getController() , $RouteSurvey->getController(true));


		return $Route;
	}

	function testGetRoute2()
	{
		$Route = new \mrblue\mvc\Route('host',[
			'type' => \mrblue\mvc\Route::TYPE_HOSTNAME,
			'equal_to' => '?test.?:action.:user.?:subuser.mrblue.com',
			'controller' => 'ApiController',
			'defaults' => [
				'action' => 'homepage'
			],
			'constraints' => [
				'user' => 'u[^.]+'
			],
			'may_terminate' => false,
			'childs' => [
				'method' => [
					'type' => \mrblue\mvc\Route::TYPE_METHOD,
					'equal_to' => 'get,POST',
					'may_terminate' => false,
					'childs' => [
						'path' => [
							'type' => \mrblue\mvc\Route::TYPE_SEGMENT,
							'equal_to' => '/api[/:action]',
							'view_template' => __DIR__ . '/test_view.php',
						],
					]
				],
			]
		]);

		$this->assertEquals('host',$Route->name);
		$this->assertEquals('ApiController',$Route->getController());
		$this->assertNull( $Route->getViewTemplate() );
		$this->assertEquals(['action'=>'homepage'],$Route->getDefaults());

		$RouteMethod = $Route->getChild('method');
		$RoutePath = $RouteMethod->getChild('path');

		$this->assertNull( $RouteMethod->getController() );
		$this->assertNull( $RouteMethod->getViewTemplate() );

		$this->assertEquals($Route->getController() , $RouteMethod->getController(true));
		$this->assertEquals($Route->getViewTemplate() , $RouteMethod->getViewTemplate(true));


		$this->assertNull( $RoutePath->getController() );
		$this->assertEquals( __DIR__ . '/test_view.php' , $RoutePath->getViewTemplate() );

		$this->assertEquals($Route->getController() , $RoutePath->getController(true));
		$this->assertNotEquals($Route->getViewTemplate() , $RoutePath->getViewTemplate(true));


		return $Route;
	}

}

