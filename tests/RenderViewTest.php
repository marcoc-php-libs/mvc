<?php
namespace Tests;

use mrblue\mvc\JsonView;
use mrblue\mvc\RenderView;

class RenderViewTest extends \PHPUnit\Framework\TestCase
{

	function testOk()
	{
		$data = ['var' => 'hello!'];
		$RenderView = new RenderView($data , __DIR__ . '/test_view.php');

		$RenderLayout = new RenderView($data , __DIR__ . '/test_layout.php');

		$RenderView->setParent($RenderLayout);

		$this->assertEquals(
			'view: <p>view var: hello!</p>',
			$RenderLayout->render()
		);

		$this->assertEquals(
			'<p>view var: hello!</p>',
			$RenderView->getOutput()
		);
	}
		
}

