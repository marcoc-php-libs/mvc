<?php

ini_set('memory_limit', '256M');

chdir( __DIR__ );

$Autoloader = include '../vendor/autoload.php';

$Autoloader->addPsr4('Tests\\MvcTest\\' , __DIR__.'/MvcTestComponents');
