<?php
namespace Tests\MvcTest;

use mrblue\mvc\AbstractController;

class DashboardController extends AbstractController
{
    function profile ()
    {
        $RouteMatch = $this->Mvc->getRouteMatch();
        $params = $RouteMatch->getParams();

        $user = (int) $params['user_id'];
        $balance = $user * 3;
        
        return [
            'user' => $user,
            'balance' => $balance
        ];
    }

    function download ()
    {
        $RouteMatch = $this->Mvc->getRouteMatch();
        $params = $RouteMatch->getParams();

        $stream = fopen('php://memory','r+');
		fwrite($stream,$params['user_id']);

        rewind($stream);

        return $stream;
    }
}