<?php
namespace Tests\MvcTest;

use mrblue\mvc\AbstractController;

class ApiController extends AbstractController
{
    function v1 ()
    {
        return [
            'status' => 'ok'
        ];
    }

    function cron ()
    {
        return 'OK';
    }
}