<?php
namespace Tests;

use mrblue\mvc\JsonView;

class JsonViewTest extends \PHPUnit\Framework\TestCase
{

	function testOk()
	{
		$data = ['a' => 1];
		$JsonView = new JsonView($data,JSON_PRETTY_PRINT);

		$this->assertEquals(JSON_PRETTY_PRINT , $JsonView->getJsonOptions());
		$this->assertInstanceOf(JsonView::class , $JsonView->setJsonOptions(JSON_UNESCAPED_SLASHES));
		$this->assertEquals(JSON_UNESCAPED_SLASHES , $JsonView->getJsonOptions());

		$this->assertEquals(json_encode($data,JSON_UNESCAPED_SLASHES),$JsonView->render());

		$this->assertInstanceOf(JsonView::class , $JsonView->setData($data));
		$this->assertInstanceOf(JsonView::class , $JsonView->setData( (object) $data));
		$this->assertInstanceOf(JsonView::class , $JsonView->setData( new \ArrayObject($data) ));

		$this->expectException( \InvalidArgumentException::class );
		$JsonView->setData( true );
	}
		
}

