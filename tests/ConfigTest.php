<?php
namespace Tests;

use mrblue\mvc\Config;

class ConfigTest extends \PHPUnit\Framework\TestCase
{

	function testNoFilters()
	{
		$config = Config::get(__DIR__.'/config_test');

		$this->assertEquals([
			'b' => [
				'override' => 'global',
				'not override' => 'local'
			],
			'a' => 'local',
			'c' => true
		],$config);
	}

	function testFilters()
	{
		$config = Config::get(__DIR__.'/config_test',['*global.php','local.php']);

		$this->assertEquals([
			'b' => [
				'override' => 'global',
				'not override' => 'local'
			],
			'a' => 'local',
		],$config);
	}

	function testRegex()
	{
		$config = Config::get(__DIR__.'/config_test',['/other/']);

		$this->assertEquals([
			'c' => true
		],$config);
	}

}

