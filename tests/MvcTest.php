<?php
namespace Tests;

use mrblue\mvc\Exception\RouteNotMatchException;
use mrblue\mvc\JsonView;
use mrblue\mvc\Mvc;
use mrblue\mvc\RenderView;
use mrblue\mvc\Response;

class MvcTest extends \PHPUnit\Framework\TestCase
{

	/**
	 * @dataProvider dataProvider
	 */
	function testOk( $config )
	{
		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['HTTP_HOST'] = 'localhost';

		$_SERVER['REQUEST_URI'] = '/api/v1';
		$Mvc = new Mvc($config);
		$this->assertEquals( $_SERVER['REQUEST_URI'] , $Mvc->url($_SERVER['REQUEST_URI']) );
		ob_start();
		$Mvc->run();
		$this->assertEquals('{"status":"ok"}',ob_get_clean());

		$_SERVER['REQUEST_URI'] = '/api/v1/cron';
		$Mvc = new Mvc($config);
		$this->assertEquals( $_SERVER['REQUEST_URI'] , $Mvc->url($_SERVER['REQUEST_URI']) );
		ob_start();
		$Mvc->run();
		$this->assertEquals('OK',ob_get_clean());

		$_SERVER['REQUEST_URI'] = '/dashboard/70';
		$Mvc = new Mvc($config);
		$this->assertEquals( $_SERVER['REQUEST_URI'] , $Mvc->url($_SERVER['REQUEST_URI']) );
		ob_start();
		$Mvc->run();
		$this->assertEquals('<html><body><h1>User 70 balance is 210</h1></body></html>',ob_get_clean());


		$_SERVER['REQUEST_URI'] = '/dashboard/70/download';
		$Mvc = new Mvc($config);
		$this->assertEquals( $_SERVER['REQUEST_URI'] , $Mvc->url($_SERVER['REQUEST_URI']) );
		ob_start();
		$Mvc->run();
		$this->assertEquals('70',ob_get_clean());
	}

	/**
	 * @dataProvider dataProvider
	 */
	function test404( $config )
	{
		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['HTTP_HOST'] = 'localhost';
		$_SERVER['REQUEST_URI'] = '/api/v2';

		$Mvc = new Mvc($config);
		ob_start();
		$Mvc->run();
		$this->assertEquals('',ob_get_clean());
		$this->assertEquals(404,http_response_code());

		$Mvc = new Mvc($config);
		$Mvc->registerEvent(Mvc::EVENT_ROUTE_EXCEPTION,function(Mvc $Mvc){
			$Response = new Response();
			$Response->setContent('not found');
			$Mvc->setResponse($Response);
		});
		ob_start();
		$Mvc->run();
		$this->assertEquals('not found',ob_get_clean());
		$this->assertEquals(404,http_response_code());
	}

	/**
	 * @dataProvider dataProvider
	 */
	function testUrlException( $config )
	{
		$Mvc = new Mvc($config);
		
		$this->expectException(RouteNotMatchException::class);

		$Mvc->url('api/v2');
	}

	function dataProvider()
	{
		$return[]['config'] = [
			'router' => [
				'routes' => [
					'apiv1' => [
						'type' => \mrblue\mvc\Route::TYPE_LITERAL,
						'equal_to' => '/api/v1',
						'controller' => MvcTest\ApiController::class,
						'defaults' => [
							'action' => 'v1'
						],
						'priority' => 10,
						'childs' => [
							'cron' => [
								'type' => \mrblue\mvc\Route::TYPE_LITERAL,
								'equal_to' => '/cron',
								'defaults' => [
									'action' => 'cron'
								]
							]
						]
					],
					'dashboard' => [
						'type' => \mrblue\mvc\Route::TYPE_SEGMENT,
						'equal_to' => '/dashboard/:user_id',
						'controller' => MvcTest\DashboardController::class,
						'view_template' => __DIR__ . '/MvcTestComponents/dashboard_profile.phtml',
						'defaults' => [
							'action' => 'profile'
						],
						'constraints' => [
							'user_id' => '\d+'
						],
						'priority' => 8,
						'childs' => [
							'download' => [
								'type' => \mrblue\mvc\Route::TYPE_LITERAL,
								'equal_to' => '/download',
								'defaults' => [
									'action' => 'download'
								]
							]
						]
					],
				]
			],
			'default_layout' => __DIR__ . '/MvcTestComponents/layout.phtml'
		];

		return $return;
	}
		
}

