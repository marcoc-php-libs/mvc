<?php
namespace Tests;

use mrblue\mvc\JsonView;
use mrblue\mvc\RenderView;
use mrblue\mvc\Response;

class ResponseTest extends \PHPUnit\Framework\TestCase
{

	function testViewOk()
	{
		$data = ['var' => 'hello!'];

		$Response = new Response();
		$Response->setType(Response::TYPE_VIEW);

		$JsonView = new JsonView($data);

		$Response->setView($JsonView);
		$this->assertEquals($JsonView,$Response->getView($JsonView));
		$this->assertEquals(json_encode($data),$Response->render());

		$RenderView = new RenderView($data , __DIR__ . '/test_view.php');
		$Response->setView($RenderView);
		$this->assertEquals($RenderView,$Response->getView($RenderView));
		$this->assertEquals('<p>view var: hello!</p>',$Response->render());
	}

	function testContentOk()
	{
		$content = 'OK';

		$Response = new Response();
		$Response->setType(Response::TYPE_CONTENT);

		$Response->setContent( $content );
		$this->assertEquals( $content ,$Response->getContent());
		$this->assertEquals( $content ,$Response->render());
	}

	function testStreamOk()
	{
		$content = 'OK';

		$Response = new Response();
		$Response->setType(Response::TYPE_STREAM);

		$stream = fopen('php://memory','r+');
		fwrite($stream,$content);

		$Response->setStream($stream);

		rewind($stream);
		$this->assertEquals( $content ,$Response->render());

		$this->assertEquals( '' ,$Response->render());

		fseek($stream,1);

		$this->assertEquals( substr($content,1) ,$Response->render());

	}
		
}

