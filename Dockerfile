ARG IMAGE

FROM $IMAGE

RUN apt-get update && \
	apt-get install -y curl wget procps zip git g++ pkg-config \
	libssl-dev zlib1g-dev libicu-dev libpq-dev

RUN pecl install mongodb-1.10.0
RUN docker-php-ext-enable mongodb
RUN docker-php-ext-install intl
	
ARG user_developer_uid=1000
RUN useradd -u $user_developer_uid -g www-data -m -s /bin/bash developer
