# MVC

```
composer require mrblue/mvc
```


File `./Controller/MainController.php`
``` php
<?php
namespace Controller;

class MainController extends \mrblue\mvc\AbstractController
{
    function getUser ()
    {
        return [ // this array will be rendered as json
            'status' => 'ok'
            'user_id' => $this->Mvc->getROuteMatch()->getParams()['user_id']
        ];
    }

    function geHtmlPage ()
    {
        return [
            'user_id' => $this->Mvc->getROuteMatch()->getParams()['user_id']
        ];
    }

    function getTxtContent()
    {
        return file_get_contents( __DIR__ . '/contents/plaintext.txt' );
    }

    function dowloadFile()
    {
        $file_target = __DIR__ . '/contents/open_data.csv';

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="export.csv"');
        header('Content-Length: '.filesize($file_target));
        return fopen( $file_target );
    }
}
```

File `./view/html_page.phtml`
``` php
<?php

/**
 * @var \mrblue\mvc\RenderView $this
 */

 $data = $this->getData();

?>
<h1>User id is <?php echo $data ['user_id'] ?></h1>
```

File `./index.php`
``` php
<?php
include 'vendor/autoload.php';

$Mvc = new Mvc([
    'router' => [
        'routes' => [
            'apiv1' => [
                'type' => \mrblue\mvc\Route::TYPE_SEGMENT,
                'equal_to' => '/api/v1/:user_id',
                'controller' => Controller\MainController::class,
                'constraints' => [
                    'user_id' => '\d+'
                ]
                'may_terminate' => false,
                'childs' => [
                    'json_example' => [
                        'type' => \mrblue\mvc\Route::TYPE_LITERAL,
                        'equal_to' => '/me',
                        'defaults' => [
                            'action' => 'getUser'
                        ],
                        'constraints' => [
                            'user_id' => '\d+'
                        ]
                    ],
                    'html_render_example' => [
                        'type' => \mrblue\mvc\Route::TYPE_LITERAL,
                        'equal_to' => '/html_page',
                        'view_template' => __DIR__ . '/view/html_page.phtml',
                        'defaults' => [
                            'action' => 'geHtmlPage'
                        ]
                    ],
                    'txt_print_example' => [
                        'type' => \mrblue\mvc\Route::TYPE_LITERAL,
                        'equal_to' => '/txt_content',
                        'defaults' => [
                            'action' => 'getTxtContent'
                        ]
                    ],
                    'download_stream_example' => [
                        'type' => \mrblue\mvc\Route::TYPE_LITERAL,
                        'equal_to' => '/download',
                        'defaults' => [
                            'action' => 'download'
                        ]
                    ],
                ]
            ]
        ]
    ]
]);

$Mvc->run();
```
