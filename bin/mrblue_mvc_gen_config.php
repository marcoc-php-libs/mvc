<?php

use mrblue\mvc\Config;

if( ! is_file('vendor/autoload.php') ){
    echo 'You must be at the some level of vendor dir';
    die(1);
}

$params = getopt("d:f:o:");

$dir = $params['d'] ?? null;
$filters = $params['f'] ?? null;
$output = $params['o'] ?? null;

if( ! $dir || ! is_string($dir) ) {
    echo "dir is an empty string";
    die(1);
}

if( $filters ){
    $filters = array_filter((array) $filters , function($value){
        return $value && is_string($value);
    });
}

if( ! $output || ! is_string($output) ) {
    echo "output is not a valid dir";
    die(1);
}

include 'vendor/autoload.php';

$config = Config::get($dir , $filters ?? [] );

file_put_contents($output,"<?php\nreturn ".var_export($config,true).';');