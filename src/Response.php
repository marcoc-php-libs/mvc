<?php
namespace mrblue\mvc;

use mrblue\mvc\Exception\RouteBadConfiguredException;

class Response
{
	CONST TYPE_CONTENT = 'CONTENT';
	CONST TYPE_STREAM = 'STREAM';
	CONST TYPE_VIEW = 'VIEW';

	private $type = self::TYPE_CONTENT;

	private $content;
	private $stream;
	/**
	 * @var \mrblue\mvc\ViewInterface
	 */
	private $View;

	public function setType( string $value )
	{
		if( ! defined('self::TYPE_'.$value) ){
			throw new \InvalidArgumentException("Type '$value' not valid");
		}

		$this->type = $value;
		return $this;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setContent( string $value )
	{
		$this->content = $value;
		$this->setType(self::TYPE_CONTENT);
		return $this;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function setStream( $stream )
	{
		if( ! is_resource($stream) ){
			throw new \InvalidArgumentException("Param 1 is not stream");
		}
		$this->setType(self::TYPE_STREAM);
		$this->stream = $stream;
		return $this;
	}

	public function getStream()
	{
		return $this->stream;
	}

	public function setView( ViewInterface $View )
	{
		$this->View = $View;
		$this->setType(self::TYPE_VIEW);
		return $this;
	}

	/**
	 * @return ViewInterface
	 */
	public function getView()
	{
		return $this->View;
	}

	public function render()
	{
		if( $this->type === self::TYPE_CONTENT ){
			return (string) $this->content;
		} elseif( $this->type === self::TYPE_STREAM ){
			if( ! $this->stream ){
				throw new \InvalidArgumentException("Stream not set");
			}
			return stream_get_contents($this->stream);
		} elseif( $this->type === self::TYPE_VIEW ){
			if( ! $this->View ){
				throw new \InvalidArgumentException("View not set");
			}
			$View = $this->View;

			if( $View instanceof RenderView ){
				while( $View->getParent() ){
					$View = $View->getParent();
				}
			}

			return $View->render();
		}
	}

}

