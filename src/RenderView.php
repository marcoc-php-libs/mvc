<?php

namespace mrblue\mvc;

class RenderView extends AbstractView {
    private $name;

    private $template;
    private $output;

    private $rendered;
    private $Parent;

    /**
     * @var \mrblue\mvc\RenderView[]
     */
    private $Childs = [];

    /**
     * @var \mrblue\mvc\Mvc
     */
    private $Mvc;

    static private $pageTitle = '';
    static private $pageDescription = '';
    static private $pageKeywords = '';
    static private $vars = [];

    static private $jsScript = '';
    static private $jsScriptOnload = '';
    static private $style = '';
    static private $captured = [];

    static private $jsonArrays = [];
    static private $jsonObjects = [];

    public function __construct($data, string $template, string $name = 'view') {
        $this->setData($data);
        $this->template = $template;
        $this->name = $name;
    }

    public function render() {
        foreach ($this->Childs as $Child) {
            $Child->render();
        }

        if ($this->rendered) {
            return $this->output;
        }

        ob_start();
        include $this->template;
        $this->output = ob_get_clean();

        $this->rendered = true;

        return $this->output;
    }

    public function getOutput() {
        if (! $this->rendered) {
            throw new \BadMethodCallException("View has not yet been rendered. Call render() before.");
        }

        return $this->output;
    }

    public function addChild(RenderView $Child) {
        $name = $Child->getName();

        if (isset($this->Childs[$name]) && ($this->Childs[$name] !== $Child)) {
            throw new \RuntimeException("Child with name '$name' just exists");
        }
        $this->Childs[$name] = $Child;
        $Child->setParent($this);
        return $this;
    }

    public function hasChild(string $name) {
        return isset($this->Childs[$name]);
    }

    /**
     * @return \mrblue\mvc\RenderView
     * @throws \DomainException if child with given name not exists
     */
    public function getChild(string $name) {
        if (! isset($this->Childs[$name])) {
            throw new \DomainException("Child '$name' not exists");
        }

        return $this->Childs[$name];
    }

    /**
     * @param \mrblue\mvc\RenderView|null $Parent set or unset Parent
     */
    public function setParent(?RenderView $Parent = null) {
        if ($Parent) {
            if (! $Parent->hasChild($this->name)) {
                $Parent->addChild($this);
            }

            // Mvc Propagation
            if ($Parent->getMvc() && ! $this->Mvc) {
                $this->Mvc = $Parent->getMvc();
            } elseif (! $Parent->getMvc() && $this->Mvc) {
                $Parent->setMvc($this->Mvc);
            }
        }

        $this->Parent = $Parent;
        return $this;
    }

    /**
     * @return \mrblue\mvc\RenderView|null
     */
    public function getParent() {
        return $this->Parent;
    }

    public function getName() {
        return $this->name;
    }

    public function getTemplate() {
        return $this->template;
    }

    public function setTemplate(string $template) {
        $this->template = $template;
        return $this;
    }

    public function getMvc() {
        return $this->Mvc;
    }

    public function setMvc(Mvc $Mvc) {
        $this->Mvc = $Mvc;
        return $this;
    }

    /**
     * @deprecated 0.6.2 use self::setHTMLContent() and self::getHTMLContent()
     */
    static function pageTitle(?string $title = null) {
        if ($title === null) {
            return self::$pageTitle;
        } else {
            self::$pageTitle = $title;
            return static::class;
        }
    }

    /**
     * @deprecated 0.6.2 use self::setHTMLContent() and self::getHTMLContent()
     */
    static function pageDescription(?string $description = null) {
        if ($description === null) {
            return self::$pageDescription;
        } else {
            self::$pageDescription = $description;
            return static::class;
        }
    }

    /**
     * @deprecated 0.6.2 use self::setHTMLContent() and self::getHTMLContent()
     */
    static function pageKeywords(?string $keywords = null) {
        if ($keywords === null) {
            return self::$pageKeywords;
        } else {
            self::$pageKeywords = $keywords;
            return static::class;
        }
    }

    static function setHTMLContent(string $varname, string $value) {
        self::$vars[$varname] = htmlentities($value);
    }

    static function getHTMLContent(string $varname) {
        return self::$vars[$varname] ?? null;
    }

    static function setVar(string $varname, $value) {
        self::$vars[$varname] = $value;
    }

    static function getVar(string $varname) {
        return self::$vars[$varname] ?? null;
    }

    /**
     * Simply call method ob_start()
     */
    static function startCapture() {
        ob_start();
    }

    /**
     * @deprecated 0.6.2 use self::getCaptured()
     */
    static function jsScript() {
        return self::getCaptured('jsScript');
    }
    /**
     * @deprecated 0.6.2 use self::capture()
     */
    static function jsScriptCapture() {
        self::capture('jsScript');
    }

    /**
     * @deprecated 0.6.2 use self::getCaptured()
     */
    static function jsScriptOnload() {
        return self::getCaptured('jsScriptOnload');
    }
    /**
     * @deprecated 0.6.2 use self::capture()
     */
    static function jsScriptOnloadCapture() {
        self::capture('jsScriptOnload');
    }

    /**
     * @deprecated 0.6.2 use self::getCaptured()
     */
    static function style() {
        return self::getCaptured('style');
    }
    /**
     * @deprecated 0.6.2 use self::capture()
     */
    static function styleCapture() {
        self::capture('style');
    }

    static function getCaptured(string $name) {
        return self::$captured[$name] ?? '';
    }
    static function capture(string $name) {
        if (empty(self::$captured[$name])) {
            self::$captured[$name] = '';
        }
        self::$captured[$name] .= ob_get_clean();
    }

    static function jsonArrayAdd(string $name, $value) {
        self::$jsonArrays[$name][] = $value;
    }
    static function jsonArrayUnset(string $name) {
        unset(self::$jsonArrays[$name]);
    }
    static function jsonArrayRender(string $name, int $json_encode_options = 0) {
        return json_encode(self::$jsonArrays[$name] ?? [], $json_encode_options);
    }

    static function jsonObjectSet(string $name, string $key, $value) {
        self::$jsonObjects[$name][$key] = $value;
    }
    static function jsonObjectUnset(string $name) {
        unset(self::$jsonObjects[$name]);
    }
    static function jsonObjectRender(string $name, int $json_encode_options = 0) {
        return json_encode(self::$jsonObjects[$name] ?? ((object)[]), $json_encode_options);
    }
}
