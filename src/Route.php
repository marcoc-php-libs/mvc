<?php

namespace mrblue\mvc;

use LogicException;
use mrblue\mvc\Exception\RouteNotMatchException;
use SplPriorityQueue;

class Route {
	const TYPE_METHOD = 'METHOD';
	const TYPE_LITERAL = 'LITERAL';
	const TYPE_SEGMENT = 'SEGMENT';
	const TYPE_HAYSTACK = 'HAYSTACK';
	const TYPE_HOSTNAME = 'HOSTNAME';

	/**
	 * @var Route
	 */
	private $Parent = null;

	public $name;
	public $type;
	public $equal_to;
	private $controller;
	private $view_template;
	private $constraints = [];
	private $haystack = [];
	private $defaults = [];
	public $priority = 1;
	public $may_terminate = true;
	public $required = true;
	private $sitemap = null;
	public $sitemap_changefreq = 'monthly';
	/**
	 * @var \SplPriorityQueue
	 */
	protected $Childs = [];

	private $route_regex;

	private $build_regex = true;

	public function __construct(string $name, array $options) {
		$this->name = $name;
		$this->type = constant('self::TYPE_' . $options['type']);
		$this->equal_to = $options['equal_to'];
		$this->setController($options['controller'] ?? null);
		$this->setViewTemplate($options['view_template'] ?? null);
		$this->setConstraints($options['constraints'] ?? []);
		$this->setHaystack($options['haystack'] ?? []);
		$this->setDefauts($options['defaults'] ?? []);
		$this->priority = intval($options['priority'] ?? 1);
		$this->may_terminate = boolval($options['may_terminate'] ?? $this->may_terminate);
		$this->required = boolval($options['required'] ?? $this->required);
		$this->sitemap = isset($options['sitemap']) ? boolval($options['sitemap']) : null;
		$this->sitemap_changefreq = (string)($options['sitemap_changefreq'] ?? $this->sitemap_changefreq);

		$this->Childs = new SplPriorityQueue;

		if (isset($options['childs'])) {
			foreach ($options['childs'] as $name => $Child) {
				$this->addChild($Child, $name);
			}
		}
	}

	public function setController(?string $value = null) {
		if (!$value && $value !== null) {
			throw new \InvalidArgumentException("Controller value must be a non empty string");
		}

		$this->controller = $value;

		return $this;
	}

	public function getController($include_parents = false) {
		if ($include_parents) {
			return $this->controller ?? ($this->Parent ? $this->Parent->getController(true) : null);
		} else {
			return $this->controller;
		}
	}

	public function setViewTemplate(?string $value = null) {
		if (!$value && $value !== null) {
			throw new \InvalidArgumentException("view_template value must be a non empty string");
		}

		if ($value) {
			$value = realpath($value);
			if (!$value || !is_file($value)) {
				throw new \InvalidArgumentException("view_template file given in Route '" . $this->name . "' not exists");
			}
		}

		$this->view_template = $value;

		return $this;
	}

	public function getViewTemplate() {
		return $this->view_template;
	}

	public function setConstraints(array $values) {
		foreach ($values as $key => $value) {
			$this->setConstraint($key, $value);
		}

		return $this;
	}

	public function setConstraint(string $key, ?string $value = null) {
		if (!preg_match("/^[a-zA-Z][a-zA-Z0-9_\-]*$/", $key)) {
			throw new \InvalidArgumentException("Key value '$key' not valid");
		}
		if ($value === null) {
			unset($this->constraints[$key]);
		} elseif (!$value) {
			throw new \InvalidArgumentException("Constraint regex can not be empty");
		} else {
			$this->constraints[$key] = $value;
		}

		$this->build_regex = true;

		return $this;
	}

	public function getConstraints($include_parents = false) {
		if ($include_parents) {
			return $this->constraints + ($this->Parent ? $this->Parent->getConstraints(true) : []);
		} else {
			return $this->constraints;
		}
	}


	public function setHaystack(array $haystack) {
		$this->haystack = $haystack;
		return $this;
	}

	public function getHaystack() {
		return $this->haystack;
	}

	public function setDefauts(array $values) {
		foreach ($values as $key => $value) {
			$this->setDefault($key, $value);
		}

		return $this;
	}

	public function setDefault(string $key, ?string $value = null) {
		if (!preg_match("/^[a-zA-Z][a-zA-Z0-9_\-]*$/", $key)) {
			throw new \InvalidArgumentException("Key value '$key' not valid");
		}
		if ($value === null) {
			unset($this->defaults[$key]);
		} else {
			$this->defaults[$key] = $value;
		}

		return $this;
	}

	public function getDefaults($include_parents = false) {
		if ($include_parents) {
			return $this->defaults + ($this->Parent ? $this->Parent->getDefaults(true) : []);
		} else {
			return $this->defaults;
		}
	}

	public function setParent(Route $Parent) {
		$this->Parent = $Parent;

		return $this;
	}

	public function getParent() {
		return $this->Parent;
	}

	public function isSitemapEnabled($include_parents = false) {
		if ($include_parents) {
			return isset($this->sitemap) ? (bool) $this->sitemap : ($this->Parent ? $this->Parent->isSitemapEnabled(true) : false);
		} else {
			return (bool) $this->sitemap;
		}
	}

	public function addChild($Child, string $name) {
		if (!$Child instanceof self) {
			$Child = new self($name, $Child);
		}

		$Child->setParent($this);
		$this->Childs->insert($Child, $Child->priority);
		return $this;
	}

	/**
	 * @return \mrblue\mvc\RouteMatch
	 * @throws \mrblue\mvc\Exception\RouteNotMatchException
	 */
	public function match(string $path, string $method, string $hostname, array $params = []) {
		if ($this->type === self::TYPE_SEGMENT && $this->build_regex) {
			$this->route_regex = $this->segmentsToRegex($this->equal_to);
			$this->build_regex = false;
		} elseif ($this->type === self::TYPE_HAYSTACK && $this->build_regex) {
			$this->route_regex = $this->haystackToRegex($this->equal_to);
			$this->build_regex = false;
		} elseif ($this->type === self::TYPE_HOSTNAME && $this->build_regex) {
			$this->route_regex = $this->hostnameRuleToRegex($this->equal_to);
			$this->build_regex = false;
		}

		$method = strtoupper($method);

		$path_length = strlen($path);
		if ($this->type === self::TYPE_LITERAL) {
			$eq_to_length = strlen($this->equal_to);
			if (strncmp($path, $this->equal_to, $eq_to_length) !== 0) {
				throw new RouteNotMatchException();
			}

			$path_overflow = substr($path, $eq_to_length);

			if ($path_overflow || !$this->may_terminate) {
				goto childs;
			} else {
				return new RouteMatch($this, $method, $path, $hostname, $params); // success
			}
		} elseif ($this->type === self::TYPE_SEGMENT) {
			$matches = [];
			if (!preg_match($this->route_regex, $path, $matches)) {
				throw new RouteNotMatchException();
			}

			$params = array_filter($matches, function ($value, $key) {
				return is_string($key) && ($value !== '');
			}, ARRAY_FILTER_USE_BOTH) + $params;

			$eq_to_length = strlen($matches[0]);
			$path_overflow = substr($path, $eq_to_length);

			if ($path_overflow || !$this->may_terminate) {
				goto childs;
			} else {
				return new RouteMatch($this, $method, $path, $hostname, $params); // success
			}
		} elseif ($this->type === self::TYPE_HAYSTACK) {

			$matches = [];
			if (!preg_match($this->route_regex, $path, $matches)) {
				throw new RouteNotMatchException();
			}

			$this_params = array_filter($matches, function ($value, $key) {
				return is_string($key) && ($value !== '');
			}, ARRAY_FILTER_USE_BOTH);

			if (count($this_params) !== 1) {
				throw new \InvalidArgumentException('haystack route must has exactly one replaceable element');
			}

			$params = $this_params + $params;

			if (!in_array(current($this_params), $this->haystack, true)) {
				throw new RouteNotMatchException();
			}

			$eq_to_length = strlen($matches[0]);
			$path_overflow = substr($path, $eq_to_length);

			if ($path_overflow || !$this->may_terminate) {
				goto childs;
			} else {
				return new RouteMatch($this, $method, $path, $hostname, $params); // success
			}

			/*$parts = explode('/', $path);
			$slash_pre = (substr($this->equal_to, 0, 1) === '/') ? '/' : '';
			$slash_post = (substr($this->equal_to, -1, 1) === '/') ? '/' : '';
			$param_name = trim($this->equal_to, '/');

			if (!$param_name) {
				throw new \InvalidArgumentException('equal_to can not be empty');
			}

			if ($slash_pre && $slash_post) {
				throw new \InvalidArgumentException('slash can not be either pre and post');
			}

			if ($slash_pre) {
				$target_part = $parts[1] ?? '';
				$eq_to_length_sum = 1;
			} else {
				$target_part = $parts[0];
				$eq_to_length_sum = $slash_post ? 1 : 0;
			}

			if (in_array($target_part, $this->haystack, true)) {

				if (($slash_pre || $slash_post) && !isset($parts[1])) {
					throw new RouteNotMatchException();
				}

				$eq_to_length = strlen($target_part) + $eq_to_length_sum;
				$params[$param_name] = $target_part;
			} else {
				$eq_to_length = 0;
				if ($this->required) {
					throw new RouteNotMatchException();
				}
			}

			$path_overflow = substr($path, $eq_to_length);
			if ($path_overflow || !$this->may_terminate) {
				goto childs;
			} else {
				return new RouteMatch($this, $method, $path, $hostname, $params); // success
			}*/
		} elseif ($this->type === self::TYPE_METHOD) {
			$in = array_map(function ($value) {
				return strtoupper($value);
			}, explode(',', $this->equal_to));

			if (!in_array($method, $in)) {
				throw new RouteNotMatchException();
			}

			$path_overflow = $path;

			if ($path_overflow || !$this->may_terminate) {
				goto childs;
			} else {
				return new RouteMatch($this, $method, $path, $hostname, $params); // success
			}
		} elseif ($this->type === self::TYPE_HOSTNAME) {
			$matches = [];
			if (!preg_match($this->route_regex, $hostname, $matches)) {
				throw new RouteNotMatchException();
			}

			$params = array_filter($matches, function ($value, $key) {
				return is_string($key) && $value;
			}, ARRAY_FILTER_USE_BOTH) + $params;

			$path_overflow = $path;

			if ($path_overflow || !$this->may_terminate) {
				goto childs;
			} else {
				return new RouteMatch($this, $method, $path, $hostname, $params); // success
			}
		} else {
			throw new \LogicException("Unknown route type '{$this->type}'");
		}

		childs:


		$Childs = clone $this->Childs;

		foreach ($Childs as $ChildRoute) {
			try {
				$RouteMatch = $ChildRoute->match($path_overflow, $method, $hostname, $params);
				$RouteMatch->setPath($path);
				return $RouteMatch;
			} catch (RouteNotMatchException $th) {
				continue;
			}
		}

		throw new RouteNotMatchException();
	}

	/**
	 * @return boolen
	 */
	public function hasChild(string $name) {
		try {
			$this->getChild($name);
		} catch (\DomainException $th) {
			return false;
		}

		return true;
	}

	/**
	 * @return \mrblue\mvc\Route
	 * @throws \DomainException
	 */
	public function getChild(string $name) {
		$Childs = clone $this->Childs;

		foreach ($Childs as $Child) {
			if ($Child->name === $name) {
				return $Child;
			}
		}

		throw new \DomainException("Child '$name' not exists");
	}

	/**
	 * @return \SplPriorityQueue
	 */
	public function getChilds() {
		return $this->Childs;
	}

	private function segmentsToRegex(string $segments) {
		$pieces = preg_split("/([\[\]]+)/", $segments, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

		$regex = '/^';
		$in = false;
		foreach ($pieces as $piece) {
			if ($piece == '[') {
				if ($in) {
					throw new LogicException("Segment not required just started");
				}
				$in = true;
			} elseif ($piece == ']' || $piece == '][') {
				if (!$in) {
					throw new LogicException("Segment not required is not started");
				}
				if ($piece == ']') {
					$in = false;
				}
			} else {
				$regex .= $this->getSegmentRegex($piece, !$in);
			}
		}

		$regex .= '/';

		return $regex;
	}

	private function getSegmentRegex(string $segment, bool $required) {
		$pieces = preg_split("/(:[a-zA-Z][a-zA-Z0-9_\-]*:?)/", $segment, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

		$regex = '(?:';
		foreach ($pieces as $piece) {
			if ($piece[0] == ':') {
				$piece = trim($piece, ':');
				if (isset($this->constraints[$piece])) {
					$regex .= "(?<" . $piece . ">" . $this->constraints[$piece] . ")";
				} else {
					$regex .= "(?<" . $piece . ">[^\/]+)";
				}
			} else {
				$regex .= "(?:" . preg_quote($piece, '/') . ")";
			}
		}
		$regex .= ')' . ($required ? '' : '?');

		return $regex;
	}

	private function haystackToRegex(string $declaration) {

		$pieces = preg_split('/(\:[^\/]+)/', $declaration, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
		$regex = '';

		foreach ($pieces as $piece) {
			if ($piece[0] === ':') {
				$regex .= '(?<' . substr($piece, 1) . '>[^\/]+)';
			} else {
				$regex .= preg_quote($piece, '/');
			}
		}

		return '/^' . $regex . '/';
	}

	private function hostnameRuleToRegex(string $hostname_rule) {
		$pieces = explode('.', $hostname_rule);

		$regex = '/^';

		$size = sizeof($pieces);
		$counter = 0;

		foreach ($pieces as $piece) {
			$last = (++$counter === $size);
			$required = true;
			$dynamic = false;
			if ($piece && $piece[0] === '?') {
				$required = false;
				$piece = substr($piece, 1);
			}
			if ($piece && $piece[0] === ':') {
				$dynamic = true;
				$piece = substr($piece, 1);
			}
			if (!$piece) {
				throw new \InvalidArgumentException("Hostname rule empty piece");
			}
			if (!$required && $last) {
				throw new \LogicException("Hostname rule last piece can not be not required");
			}

			if ($dynamic) {
				if (isset($this->constraints[$piece])) {
					$regex .= "(?:(?<" . $piece . ">" . $this->constraints[$piece] . ")" . ($last ? '' : '\.') . ")" . ($required ? '' : '?');
				} else {
					$regex .= "(?:(?<" . $piece . ">[^.]+)" . ($last ? '' : '\.') . ")" . ($required ? '' : '?');
				}
			} else {
				$regex .= "(?:" . preg_quote($piece, '/') . ($last ? '' : '\.') . ")" . ($required ? '' : '?');
			}
		}

		$regex .= '$/';

		return $regex;
	}
}
