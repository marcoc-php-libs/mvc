<?php
namespace mrblue\mvc;

class MvcEvent extends Event\Event {

    public readonly Mvc $Mvc;

    function __construct( string $name , mixed $data ,  Mvc $Mvc ) {
        parent::__construct( Mvc::EVENT_PREFIX.$name , $data);
        $this->Mvc = $Mvc;
    }
}