<?php
namespace mrblue\mvc\FileManager;

abstract class AbstractManager
{
	protected $name_prefix;
	protected $model_class;
	protected $list_class;
	protected $public = false;
	protected $custom_domain = null;
	protected $private_url_ttl;
	protected $object_ttl;
}