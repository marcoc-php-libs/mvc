<?php
namespace mrblue\mvc\FileManager;

use \mrblue\mvc\File\AbstractFile;
use mrblue\mvc\FileLists\GcpStorageList;

/**
 * @property-read string $bucket GCP bucket name
 */
abstract class GcpStorageManager extends AbstractManager
{
	CONST DEFAULT_DOMAIN = 'https://storage.googleapis.com';
	
	/**
	 * @var \Google\Cloud\Storage\StorageClient
	 */
	protected $StorageClient;
	protected $bucket_name;

	/**
	 * @var \Google\Cloud\Storage\Bucket
	 */
	protected $Bucket;

	function __construct( \Google\Cloud\Storage\StorageClient $StorageClient )
	{
		$this->StorageClient = $StorageClient;
		$this->Bucket = $StorageClient->bucket($this->bucket_name);
	}
	
	public function __get($key)
	{
		if( $key === 'bucket_name' ){
			return $this->{$key};
		} else {
			throw new \Exception("Property $key not exists");
		}
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getBaseUrl()
	{
		if( $this->custom_domain ){
			return $this->custom_domain;
		} else {
			return self::DEFAULT_DOMAIN . '/' . $this->bucket_name;
		}
	}
	
	/**
	 * 
	 * @param \mrblue\mvc\File\AbstractFile|string $File File instance or File name
	 * @return string
	 */
	public function getUrl( $File )
	{
		$model_class = $this->model_class;
		if( ! $File instanceof $model_class ){
			/* @var \mrblue\mvc\File\AbstractFile $File */
			$File = new $model_class($File);
		}
		
		if( $this->public ){
			return $this->getBaseUrl() . "/$File";
		} else {
			$options = [
				'method' => 'GET',
				'version' => 'v4',
				'virtualHostedStyle' => true
			];
			if( $this->custom_domain ){
				$options['bucketBoundHostname'] = $this->custom_domain;
				$options['scheme'] = 'https';
			}
			return $this->getObjectFile($File)->signedUrl( time() + $this->private_url_ttl , $options);
		}
	}
	
	/**
	 * 
	 * @param \mrblue\mvc\File\AbstractFile|string $File File instance or File name
	 * @return \Google\Cloud\Storage\StorageObject
	 */
	public function getObjectFile( $File )
	{
		$model_class = $this->model_class;
		if( ! $File instanceof $model_class ){
			/**
			 * @var \mrblue\mvc\File\AbstractFile $File 
			 */
			$File = new $model_class($File);
		}
		
		return $this->Bucket->object( $File->__toString() );
	}
	
	/**
	 * 
	 * @param \mrblue\mvc\File\AbstractFile|string $File File instance or File name
	 * @param string|resource $resource
	 * @param array $options
	 * @return \Google\Cloud\Storage\StorageObject
	 */
	public function uploadFile( $File , $resource , array $gcs_options = [] )
	{
		$model_class = $this->model_class;
		if( ! $File instanceof $model_class ){
			/**
			 * @var \mrblue\mvc\File\AbstractFile $File 
			 */
			$File = new $model_class($File);
		}
		
		$gcs_options = [
			'name' => $File->__toString()
		] + $gcs_options;

		if( $this->object_ttl && empty($gcs_options['customTime']) ){
			$gcs_options['customTime'] = date(\DateTime::RFC3339,time() + $this->object_ttl);
		}

		return $this->Bucket->upload($resource,$gcs_options);
	}
	
	/**
	 * 
	 * @param \mrblue\mvc\File\AbstractFile|string $File File instance or File name
	 * @return bool always true
	 */
	public function deleteObject( $File )
	{
		$model_class = $this->model_class;
		if( ! $File instanceof $model_class ){
			/**
			 * @var \mrblue\mvc\File\AbstractFile $File 
			 */
			$File = new $model_class($File);
		}
		
		$this->Bucket->object($File->__toString())->delete();
		return true;
	}
	
	/**
	 * 
	 * @param string $user_prefix context prefix, different from static prefix.
	 * A context prefix can be a User id for example "158". Char "_" always follow
	 * user_prefix. It is automatically added if missing
	 * @return \mrblue\mvc\FileLists\GcpStorageList
	 */
	public function search( string $user_prefix )
	{
		if( $user_prefix && $user_prefix[-1] !== '_' ){
			$user_prefix.='_';
		}
			
		$model_class = $this->model_class;
		$list_class = $this->list_class;
		/**
		 * 
		 * @var \Intoway\Core\FileLists\GcpStorageList $List
		 */
		$List = new $list_class;

		$query = [
			'prefix' => $this->name_prefix . $user_prefix
		];

		start:

		$Objects = $this->Bucket->objects($query);
			
		foreach ($Objects as $Object){
			$name = str_replace($this->name_prefix, '', $Object->name());
			$List->add( new $model_class($name) , [
				'attributes' => [
					GcpStorageList::ATTR_GCS_OBJECT => $Object
				]
			]);
		}

		if( $Objects->nextResultToken() ){
			$query['pageToken'] = $Objects->nextResultToken();
			goto start;
		}
		
		return $List;
	}
}

