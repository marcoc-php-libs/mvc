<?php
namespace mrblue\mvc;

use mrblue\mvc\Exception\RouteBadConfiguredException;

class RouteMatch
{
	private $Route;
	private $method;
	private $path;
	private $hostname;
	private $params;

	private $controller;
	private $action;
	private $view_template;
	
	public function __construct( Route $Route , string $method , string $path , string $hostname , array $params )
	{
		$this->Route = $Route;
		$this->method = $method;
		$this->path = $path;
		$this->hostname = $hostname;

		$this->controller = $Route->getController(true);
		if( ! $this->controller ){
			throw new RouteBadConfiguredException('Controller not set');
		}

		$this->view_template = $Route->getViewTemplate(true);

		$this->params = $params + $Route->getDefaults(true);

		if( empty($this->params['action']) ){
			throw new RouteBadConfiguredException('Action not set');
		}
		$this->action = $this->params['action'];
	}

	public function getRoute()
	{
		return $this->Route;
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function getPath()
	{
		return $this->path;
	}

	public function getHostname()
	{
		return $this->hostname;
	}

	public function setPath( string $value )
	{
		return $this->path = $value;
	}

	public function getParams()
	{
		return $this->params;
	}

	public function getController()
	{
		return $this->controller;
	}

	public function getViewTemplate()
	{
		return $this->view_template;
	}

	public function getAction()
	{
		return $this->action;
	}
}

