<?php
namespace mrblue\mvc\FileLists;

abstract class GcpStorageList extends AbstractList
{
	CONST ATTR_GCS_OBJECT = 'GCSObject';
	
	/**
	 * @return NULL|\Google\Cloud\Storage\StorageObject
	 */
	public function getGcpObject()
	{
		return $this->getAttribute( self::ATTR_GCS_OBJECT );
	}
}

