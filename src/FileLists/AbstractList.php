<?php
namespace mrblue\mvc\FileLists;

use mrblue\mvc\File\AbstractFile;

abstract class AbstractList extends \mrblue\mvc\Lists\AbstractList
{
	function __construct( ...$args )
	{
		if( ! is_a($this->model_class,AbstractFile::class,true) ){
			throw new \RuntimeException($this->model_class.' not extend '.AbstractFile::class );
		}

		parent::__construct( ...$args );
	}
}

