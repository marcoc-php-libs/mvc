<?php
namespace mrblue\mvc;

class JsonView extends AbstractView
{
    private $json_options = 0;
    
    public function __construct( $data , int $json_options = 0 )
    {
        $this->setData($data);
        $this->setJsonOptions($json_options);
    }

    public function setJsonOptions( int $value )
    {
        $this->json_options = $value;
        return $this;
    }

    public function getJsonOptions()
    {
        return $this->json_options;
    }

    public function render()
    {
        return json_encode($this->data , $this->json_options);
    }
}

