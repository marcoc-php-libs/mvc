<?php
namespace mrblue\mvc\Model;

class AbstractModel
{
	/**
	 * 
	 * @param array $data pool of data where search in
	 * @param array $fields list of fields name to search
	 * @param string|null $cast @see https://www.php.net/manual/en/language.types.type-juggling.php
	 * @return array filtered pool of data
	 */
	static function getFields( array $data , array $fields , string $cast = null , string $prefix = '' )
	{
		$extracted = [];
		
		foreach( $fields as $field ){
			
			if( is_array($field) ){
				$cast_field = $field[1] ?? null;
				$field = $field[0];
			} else {
				$cast_field = null;
			}
			
			$cast_to = $cast_field ?? $cast;
			
			if( array_key_exists($field, $data) ){
				$field_prefixed = $prefix . $field;
				if( $data[$field] === null ){
					$extracted[$field_prefixed] = null;
				} else {
					$extracted[$field_prefixed] = $cast_to ? self::castTo($cast_to , $data[$field]) : $data[$field];
				}
			} else {
				throw new \InvalidArgumentException("Field '$field' missing");
			}
		}
		
		return $extracted;
	}

	/**
	 * 
	 * @param array $data pool of data where search in
	 * @param array $fields list of fields name to search
	 * @param string|null $cast @see https://www.php.net/manual/en/language.types.type-juggling.php
	 * @return array filtered pool of data
	 */
	static function getIssetFields( array $data , array $fields , string $cast = null , string $prefix = '' )
	{
		$isset = [];
		
		foreach( $fields as $field ){
			
			if( is_array($field) ){
				$cast_field = $field[1] ?? null;
				$field = $field[0];
			} else {
				$cast_field = null;
			}
			
			$cast_to = $cast_field ?? $cast;
			
			if( isset($data[$field]) ){
				$field_prefixed = $prefix . $field;
				$isset[$field_prefixed] = $cast_to ? self::castTo($cast_to , $data[$field]) : $data[$field];
			}
		}
		
		return $isset;
	}
	/**
	 *
	 * @param array $data pool of data where search in
	 * @param array $fields list of fields name to search
	 * @param string|null $cast @see https://www.php.net/manual/en/language.types.type-juggling.php
	 * @return array filtered pool of data
	 */
	static function getExistingFields( array $data , array $fields , string $cast = null , string $prefix = '' )
	{
		$existing = [];
		
		foreach( $fields as $field ){
			
			if( is_array($field) ){
				$cast_field = $field[1] ?? null;
				$field = $field[0];
			} else {
				$cast_field = null;
			}
			
			$cast_to = $cast_field ?? $cast;
			
			if( array_key_exists($field, $data) ){
				$field_prefixed = $prefix . $field;
				if( $data[$field] === null ){
					$existing[$field_prefixed] = null;
				} else {
					$existing[$field_prefixed] = $cast_to ? self::castTo($cast_to , $data[$field]) : $data[$field];
				}
			}
		}
		
		return $existing;
	}
	/**
	 *
	 * @param array $data pool of data where search in
	 * @param array $fields list of fields name to search
	 * @param string|null $cast @see https://www.php.net/manual/en/language.types.type-juggling.php
	 * @return array filtered pool of data
	 */
	static function getNotEmptyFields( array $data , array $fields , string $cast = null , string $prefix = '' )
	{
		$not_empty = [];
		
		foreach( $fields as $field ){
			
			if( is_array($field) ){
				$cast_field = $field[1] ?? null;
				$field = $field[0];
			} else {
				$cast_field = null;
			}
			
			$cast_to = $cast_field ?? $cast;
			
			if( ! empty($data[$field]) ){
				$field_prefixed = $prefix . $field;
				$not_empty[$field_prefixed] = $cast_to ? self::castTo($cast_to , $data[$field]) : $data[$field];
			}
		}
		
		return $not_empty;
	}

	/**
	 * @description If params is an empty array or has only one null element,
	 * the object will not be instantiated and null will be returned.
	 * If first param is just an instance of $class, this valure will be returned
	 * @param string $class Class name with which to instantiate the object
	 * @param array $params Array of params
	 * @return object|null
	 */
	static function newObject( string $class , array $params ) {

		if(
			! $params ||
			( current($params) === null && sizeof($params) === 1 )
		){
			return null;
		}

		if( current($params) instanceof $class ){
			return current($params);
		}

		return new $class( ...$params );
	}
	
	/**
	 * 
	 * @param mixed $cast type or class or callable @see https://www.php.net/manual/en/language.types.type-juggling.php
	 * @param mixed $var variable to convert
	 * @throws \OutOfBoundsException
	 * @return number|boolean|string|array|StdClass|NULL
	 */
	static function castTo( $cast , $var )
	{
		if( is_callable($cast) ){
			return $cast($var);
		}

		if( ! is_string($cast) ){
			throw new \InvalidArgumentException('$cast must be a callable function, a class name or a vartype');
		}

		if(  class_exists($cast) ){
			return new $cast($var);
		}

		switch ( $cast ){
			case 'int' : return (int) $var; break;
			case 'integer' : return (integer) $var; break;
			case 'bool' : return (bool) $var; break;
			case 'boolean' : return (boolean) $var; break;
			case 'float' : return (float) $var; break;
			case 'double' : return (double) $var; break;
			case 'real' : return (float) $var; break;
			case 'string' : return (string) $var; break;
			case 'array' : return (array) $var; break;
			case 'object' : return (object) $var; break;
			case 'unset' : return null; break;
		
			default: throw new \OutOfBoundsException("Cast type $cast not valid");
		}
	}

	static function get_final_class( array $data ) {

		return static::class;
	}
}

