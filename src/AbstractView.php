<?php
namespace mrblue\mvc;

abstract class AbstractView implements ViewInterface
{
    protected $data;

    public function getData()
    {
        return $this->data;
    }

    public function setData( $data )
    {
        if( ! self::isValidData($data) ){
            throw new \InvalidArgumentException("data must be an instance of \stdClass or \ArrayAccess");
        }

        $this->data = $data;
        return $this;
    }

    static public function isValidData( $data )
    {
        return is_array($data) || $data instanceof \stdClass || $data instanceof \ArrayAccess;
    }
}

