<?php
namespace mrblue\mvc;

class Config
{
    static function get( string $dir , array $filters = [] )
    {
        if( ! is_dir($dir) ){
            throw new \InvalidArgumentException("'$dir' is not valid directory");
        }

        $config = [];
        foreach( self::getFiles($dir , $filters) as $file ){
            $config = array_replace_recursive($config , include $file);
        }

        return $config;
    }

    static protected function getFiles( string $dir , array $filters = [] )
    {
        if( ! is_dir($dir) ){
            throw new \InvalidArgumentException("'$dir' is not valid directory");
        }
        $dir = realpath($dir);

        foreach($filters as $key => $filter){
            if( $filter[0] === '/' ){
                continue;
            } elseif( strpos($filter,'*') !== false ){
                $pieces = explode('*',$filter);
                $pieces = array_map(function($value){
                    return preg_quote($value,'/');
                },$pieces);
                $filters[$key] = '/^'.implode('.*' , $pieces).'$/';
            } else {
                continue;
            }
        }

        $list = scandir($dir);
        $file_php = [];
        $valid_files = [];

        foreach( $list as $item ){
            if( $item === '.' || $item === '..' ){
                continue;
            }

            if( is_dir("$dir/$item") ){
                $valid_files = array_merge($valid_files , self::getFiles("$dir/$item", $filters));
            }

            if( is_file("$dir/$item") && pathinfo($item,PATHINFO_EXTENSION) === 'php' ){
                $file_php[] = $item;
            }
        }

        if( $filters ){
            foreach( $filters as $filter ){
                foreach($file_php as $file ){
                    if( $filter[0] === '/' ){
                        if( ! preg_match($filter,$file) ){
                            continue;
                        }
                    } else {
                        if( $filter !== $file ){
                            continue;
                        }
                    }
                    $file = "$dir/$file";
                    if( ! in_array($file,$valid_files) ){
                        $valid_files[] = $file;
                    }
                }
            }
        } else {
            foreach($file_php as $file ){
                $valid_files[] = "$dir/$file";
            }
        }

        return $valid_files;
    }
}