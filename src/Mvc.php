<?php
namespace mrblue\mvc;

use mrblue\mvc\Event\Event;
use mrblue\mvc\Event\EventManager;
use mrblue\mvc\Exception\ControllerActionNotExistsException;
use mrblue\mvc\Exception\RouteNotMatchException;

class Mvc {

	CONST EVENT_PREFIX = 'mvc_';

	CONST EVENT_INIT = 'init';
	CONST EVENT_AFTER_ROUTE = 'after_route';
	CONST EVENT_AFTER_CONTROLLER = 'after_controller';
	CONST EVENT_AFTER_VIEW = 'after_view';
	
	CONST EVENT_ROUTE_EXCEPTION = 'route_exception';
	CONST EVENT_CONTROLLER_EXCEPTION = 'controller_exception';
	CONST EVENT_VIEW_EXCEPTION = 'view_exception';

	protected array $config = [];

	public readonly \SplPriorityQueue $Routes;
	public readonly EventManager $EventManager;

	protected ?string $default_layout = null;

	protected ?RouteMatch $RouteMatch = null;

	protected ?\Throwable $Exception = null;

	protected ?string $controller_class = null;

	protected ?string $controller_action = null;

	protected ?\mrblue\mvc\AbstractController $Controller = null;

	/**
	 * @var \mrblue\mvc\AbstractController[]
	 */
	protected $ControllerInstances = [];

	protected ?\mrblue\mvc\Response $Response = null;

	/**
	 * @deprecated
	 */
	private $events = [];

	protected bool $stop = false;
	
	public function __construct( array $config ) {

		$this->config = $config;

		$router = $config['router'] ?? [];
		$routes = $router['routes'] ?? null;

		if( ! is_array($routes) || ! $routes ){
			throw new \InvalidArgumentException('router.routes config must be a non empty array');
		}

		$this->Routes = new \SplPriorityQueue;
		foreach( $routes as $name => $options ){
			if( $options instanceof Route ){
				$Route = $options;
			} else {
				$Route = new Route($name,$options);
			}
			$this->Routes->insert( $Route , $Route->priority );
		}

		if( isset($config['default_layout']) ){
			$this->default_layout = realpath($config['default_layout']);
			if( ! $this->default_layout || ! is_file($this->default_layout) ){
				throw new \InvalidArgumentException('default_layout "'.$config['default_layout'].'" not exists or is not a file');
			}
		}

		$this->EventManager = new EventManager;
	}

	/**
	 * 
	 * @deprecated use EventManager instead
	 */
	public function registerEvent( string $event_name , $callback , int $priority = 1 )
	{
		if( ! defined('self::EVENT_'.strtoupper($event_name)) ){
			throw new \InvalidArgumentException("Event '$event_name' not exists");
		}

		$event_name = constant('self::EVENT_'.strtoupper($event_name));
		if( ! isset($this->events[$event_name]) ){
			$this->events[$event_name] = new \SplPriorityQueue;
		}

		$this->events[$event_name]->insert( $callback , $priority );

		return $this;
	}

	private function runEvents( string $event_name ) {

		$num_events = 0;

		/**
		 * @deprecated event management
		 */
		if( isset($this->events[$event_name]) ){
			$events = clone $this->events[$event_name];
			foreach( $events as $event_callback ){
				++$num_events;
				$event_callback( $this );
				if( $this->stop ){
					break;
				}
			}
		}

		/**
		 * new event management
		 */
		$this->EventManager->trigger(new MvcEvent( $event_name,[],$this));

		return $num_events;
	}

	public function stop( bool $value )
	{
		$this->stop = $value;
		return $this;
	}

	public function run()
	{
		// INIT
		$this->runEvents(self::EVENT_INIT);

		if( $this->stop ){
			return;
		}

		route:

		$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		$method = $_SERVER['REQUEST_METHOD'] ?? '';
		$hostname = $_SERVER['HTTP_HOST'] ?? '';

		$RouteMatch = $RouteException = null;
		foreach( $this->getRoutes() as $Route ){
			try {
				$RouteMatch = $Route->match($path , $method , $hostname);
				break;
			} catch ( RouteNotMatchException $RouteException) {
				continue;
			} catch( \Throwable $RouteException ){
				break;
			}
		}

		if( ! $RouteMatch ){
			$this->Exception = $RouteException;
			if( $RouteException instanceof RouteNotMatchException ){
				http_response_code(404);
			} else {
				http_response_code(500);
			}
			$this->runEvents(self::EVENT_ROUTE_EXCEPTION);

			if( ! $this->Response ){
				$this->Response = new Response(Response::TYPE_CONTENT);
			}

			if( $this->stop ){
				return;
			}

			goto controller;
		} else {
			$this->RouteMatch = $RouteMatch;
			$this->setControllerClass( $RouteMatch->getController() );
			$this->setControllerAction( $RouteMatch->getAction() );
			$view_template = $RouteMatch->getViewTemplate();

			$this->Response = new Response(Response::TYPE_VIEW);

			if( $view_template ){
				$View = new RenderView([],$view_template);
				$View->setMvc($this);

				if( $this->default_layout ){
					$LayoutView = new RenderView([],$this->default_layout,'layout');
					$View->setParent($LayoutView);
				}
			} else {
				$View = new JsonView(new \stdClass);
			}

			$this->Response->setView($View);
		}

		controller:

		if( ! $this->controller_class || ! $this->controller_action ){
			goto view;
		}

		$controller_class = $this->controller_class;
		$controller_action = $this->controller_action;

		$this->Controller = $this->getControllerInstance($controller_class);

		/**
		 * --- EVENT ---
		 */
		$this->runEvents(self::EVENT_AFTER_ROUTE);

		if( $this->stop ){
			return;
		}
		/**
		 * --- END ---
		 */

		$controller_methods = get_class_methods($controller_class);
		if( ! in_array($controller_action , $controller_methods) ){
			
			if( in_array('fallback' , $controller_methods) ){
				$controller_action = 'fallback';
			} else {
				http_response_code(500);
				$this->getResponse()->setContent('');
				$this->Exception = new ControllerActionNotExistsException("Action '$controller_action' not exists and default action is not declared");
				$this->runEvents(self::EVENT_CONTROLLER_EXCEPTION);

				if( $this->stop ){
					return;
				}

				goto view;
			}

		}

		try {
			$controller_return = $this->Controller->$controller_action();
		} catch (\Throwable $th) {
			http_response_code(500);
			$this->getResponse()->setContent('');
			$this->Exception = $th;
			$this->runEvents(self::EVENT_CONTROLLER_EXCEPTION);

			if( $this->stop ){
				return;
			}

			goto view;
		}

		if( $controller_return instanceof Response ){
			$this->Response = $controller_return;
		} elseif( $controller_return instanceof ViewInterface ){
			$this->Response->setView($controller_return);
		} elseif( AbstractView::isValidData($controller_return)  ){
			if( $this->Response->getType() === Response::TYPE_VIEW ){
				$View = $this->Response->getView();
				if( $View ){
					$View->setData($controller_return);
				} else {
					$this->Response->setView( new JsonView($controller_return) );
				}
			} else {
				$this->Response->setView( new JsonView($controller_return) );
			}
		} elseif( is_resource($controller_return) ){
			$this->Response->setStream($controller_return);
		} else {
			$this->Response->setContent( (string) $controller_return );
		}

		
		if( $this->Response->getType() === Response::TYPE_VIEW ){
			$View = $this->Response->getView();
			if( $View instanceof JsonView ){
				header('Content-Type: application/json');
			} else {
				if( ! $this->hasHeader('Content-Type') ){
					header('Content-Type: text/html; charset=UTF-8');
				}
			}
		}

		view:

		/**
		 * --- EVENT ---
		 */
		$this->runEvents(self::EVENT_AFTER_CONTROLLER);

		if( $this->stop ){
			return;
		}
		/**
		 * --- END ---
		 */


		echo $this->Response->render();

		$this->runEvents(self::EVENT_AFTER_VIEW);
	}

	/**
	 * @return \SplPriorityQueue
	 */
	public function getRoutes( $clone = true )
	{
		return $clone ? clone $this->Routes : $this->Routes;
	}

	/**
	 * @return \mrblue\mvc\RouteMatch|null
	 */
	public function getRouteMatch()
	{
		return $this->RouteMatch;
	}

	public function setControllerClass( string $value )
	{
		if( ! is_a($value , AbstractController::class , true) ){
			throw new \InvalidArgumentException("'$value' is not a valid controller");
		}

		$this->controller_class = $value;
		return $this;
	}

	public function getControllerClass()
	{
		return $this->controller_class;
	}

	public function setControllerAction( string $value )
	{
		$this->controller_action = $value;
		return $this;
	}

	public function getControllerAction()
	{
		return $this->controller_action;
	}

	/**
	 * @return \mrblue\mvc\AbstractController|null
	 */
	public function getController()
	{
		return $this->Controller;
	}

	/**
	 * @return \mrblue\mvc\Response|null
	 */
	public function getResponse()
	{
		return $this->Response;
	}

	/**
	 * @param \mrblue\mvc\Response $Response
	 */
	public function setResponse( Response $Response )
	{
		$this->Response = $Response;
		return $this;
	}

	/**
	 * @return \Throwable|nul
	 */
	public function getException()
	{
		return $this->Exception;
	}

	public function instantiateController( string $controller_class ){

		$instance = new $controller_class;
		$instance->setMvc($this);
		return $instance;
	}

	public function getControllerInstance( string $controller_class ){

		if( ! isset($this->ControllerInstances[$controller_class]) ){
			$this->ControllerInstances[$controller_class] = $this->instantiateController($controller_class);
		}

		return $this->ControllerInstances[$controller_class];
	}

	public function url( string $path , array $query = [] )
	{
		$hostname = $_SERVER['HTTP_HOST'] ?? '';

		foreach( $this->getRoutes() as $Route ){
			try {
				$Route->match($path , 'GET' , $hostname);
				return $path . ( $query ? '?' . http_build_query($query) : '');
			} catch( \Throwable $RouteException ){
				continue;
			}
		}

		throw $RouteException;
	}

	public function fullUrl( string $schema , string $hostname , string $path , array $query = [] )
	{
		if( $schema && $schema[-1] != '/' ){
			$schema.= '://';
		}
		foreach( $this->getRoutes() as $Route ){
			try {
				$Route->match($path , 'GET' , $hostname);
				return $schema . $hostname . $path . ( $query ? '?' . http_build_query($query) : '');
			} catch( \Throwable $RouteException ){
				continue;
			}
		}

		throw $RouteException;
	}

	public function getHeaders( bool $lowercase = true) {
		$list = [];
		foreach( headers_list() as $header ){
			list($name , $value) = array_pad( explode(':',$header,2) , 2 , '');
			if( $lowercase ){
				$name = strtolower($name);
			}
			$list[ trim($name) ] = ltrim($value);
		}
		return $list;
	}
	public function hasHeader( string $name , bool $case_sensitive = false ) {
		if( ! $case_sensitive ){
			$name = strtolower($name);
		}
		$list = $this->getHeaders( ! $case_sensitive );
		return isset($list[$name]);
	}
}

