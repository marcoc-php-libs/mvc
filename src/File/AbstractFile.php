<?php
namespace mrblue\mvc\File;

/**
 * 
 * @property-read string|null $user_prefix contain firt part of the name.
 * Represents the object containing the file.
 * Its value cannot contain the _ character because it is the separator from the file_id.
 * For example if the file belongs to survey 250 the value will be 250.
 * If the file belongs to user "500" and subuser "1288" the value could be "500-1288",
 * but not "500_1288" !
 * 
 * @property-read string $file_id contain second part of the name.
 * Represents the file unique id in the "user_prefix" context.
 * Unlike user prefix you can use the "_" character.
 * For example a valid id can be (using a file checksum) c13c76bffd8ecaea1fa9290423ee2661.jpg
 * 
 * @property-read string $name contain full name
 * 
 * @property-read string|null $url contain network url. This property is null by default
 * and can be set manually by application using FileManager method getUrl(). This property is not
 * settable during construction
 */
abstract class AbstractFile
{	
	protected $user_prefix;
	
	protected $file_id;

	protected $url;
	
	/**
	 * 
	 * @param string|array $config contain user_prefix or name (full name). If full name is passed, the constructor
	 * will automatically parse it in $user_prefix and $file_id. Also array can be passed
	 * @param string|null $file_id null if full name is passed in previous param
	 * @throws \InvalidArgumentException
	 * @see \Intoway\Core\File\AbstractFile::$user_prefix
	 * @see \Intoway\Core\File\AbstractFile::$file_id
	 */
	public function __construct( $config , string $file_id = null )
	{
		if( func_num_args() === 1 ){
			if( is_string($config) ){
				$name = $config;
			} elseif( is_array($config) ){
				$name = $config['name'] ?? null;
				$user_prefix = $config['user_prefix'] ?? null;
				$file_id = $config['file_id'] ?? null;
			}
		} elseif( func_num_args() === 2 ) {
			$user_prefix = (string) $config;
		} else {
			throw new \InvalidArgumentException('Function accept 1 or 2 arguments, '.func_num_args().' given.');
		}

		if( isset($name) ){
			$pieces = explode('_', $name, 2);
			if( count($pieces) === 2 ){
				list($this->user_prefix,$this->file_id) = $pieces;
			} else {
				throw new \InvalidArgumentException("The file name must be in the format {user_prefix}_{file_id}. '$name' given");
			}
		} elseif( isset($user_prefix,$file_id) ) {
			$this->user_prefix = $user_prefix;
			$this->file_id = $file_id;
		} else {
			throw new \InvalidArgumentException('unexpected situation.');
		}
		
		if( ! $this->user_prefix || ! is_string($this->user_prefix) ){
			throw new \InvalidArgumentException('user_prefix must be a not empty string, '.var_export($this->user_prefix,true).' given.');
		}
		
		if( ! $this->file_id || ! is_string($this->file_id) ){
			throw new \InvalidArgumentException('file_id must be a not empty string, '.var_export($this->file_id,true).' given.');
		}
	}
	
	public function __get( $key )
	{
		if( in_array($key, ['user_prefix','file_id','url'])){
			return $this->{$key};
		} elseif( $key === 'name' ){
			return $this->getName();
		} else {
			throw new \DomainException("Property $key not exists");
		}
	}
	
	public function __toString()
	{
		return $this->getNamePrefix() . $this->getName();
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getName()
	{
		return (isset($this->user_prefix)?$this->user_prefix.'_':'').$this->file_id;
	}

	public function export()
	{
		return [
			'name' => $this->getName(),
			'user_prefix' => $this->user_prefix,
			'file_id' => $this->file_id,
			'url' => $this->url
		];
	}

	public function setUrl( string $url ) {
		$this->url = $url;
		return $this;
	}

	static function get_final_class( array $data ) {

		return static::class;
	}
	
	abstract public function getNamePrefix();
}

