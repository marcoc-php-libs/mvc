<?php
namespace mrblue\mvc\Logging;

use ErrorException;
use mrblue\mvc\Utility\Env;

class GcpLogger
{
	
	/**
	 *
	 * @var \Google\Cloud\Logging\LoggingClient
	 */
	static private $LogClient;
	
	static private $gcp_resource;
	
	static private $http_request;

	static public $global_labels = [];
	
	/**
	 *
	 * @return \Google\Cloud\Logging\LoggingClient
	 */
	static function getLogClient()
	{
		if( ! self::$LogClient ){
			self::$LogClient = new \Google\Cloud\Logging\LoggingClient([
				'transport' => 'rest'
			]);
		}
		
		return self::$LogClient;
	}
	
	static function setLogClient( \Google\Cloud\Logging\LoggingClient $LogClient )
	{
		self::$LogClient = $LogClient;
	}
	
	/**
	 *
	 * @return boolean|string[]
	 */
	static function getGcpResource()
	{
		if( null === self::$gcp_resource ){
			if( ! empty($_SERVER['GAE_SERVICE']) ){
				self::$gcp_resource = [
					'type' => 'gae_app',
					'labels' => [
						'module_id' => $_SERVER['GAE_SERVICE']
					]
				];
				if( ! empty($_SERVER['GOOGLE_CLOUD_PROJECT']) ){
					self::$gcp_resource['labels']['project_id'] = $_SERVER['GOOGLE_CLOUD_PROJECT'];
				}
				if( ! empty($_SERVER['GAE_VERSION']) ){
					self::$gcp_resource['labels']['version_id'] = $_SERVER['GAE_VERSION'];
				}
				if( ! empty($_SERVER['GOOGLE_CLOUD_REGION']) ){
					self::$gcp_resource['labels']['region'] = $_SERVER['GOOGLE_CLOUD_REGION'];
				}
			} elseif( ! empty($_SERVER['GCR_SERVICE']) ) {
				self::$gcp_resource = [
					'type' => 'cloud_run_revision',
					'labels' => [
						'service_name' => $_SERVER['GCR_SERVICE']
					]
				];
				if( ! empty($_SERVER['GOOGLE_CLOUD_PROJECT']) ){
					self::$gcp_resource['labels']['project_id'] = $_SERVER['GOOGLE_CLOUD_PROJECT'];
				}
				if( ! empty($_SERVER['GCR_REVISION']) ){
					self::$gcp_resource['labels']['revision_name'] = $_SERVER['GCR_REVISION'];
				}
				if( ! empty($_SERVER['GOOGLE_CLOUD_REGION']) ){
					self::$gcp_resource['labels']['region'] = $_SERVER['GOOGLE_CLOUD_REGION'];
				}
			}  elseif( ! empty($_SERVER['K_SERVICE']) ) {
				self::$gcp_resource = [
					'type' => 'cloud_function',
					'labels' => [
						'function_name' => $_SERVER['K_SERVICE']
					]
				];
				if( ! empty($_SERVER['GOOGLE_CLOUD_PROJECT']) ){
					self::$gcp_resource['labels']['project_id'] = $_SERVER['GOOGLE_CLOUD_PROJECT'];
				}
				if( ! empty($_SERVER['K_REVISION']) ){
					self::$gcp_resource['labels']['revision_name'] = $_SERVER['K_REVISION'];
				}
				if( ! empty($_SERVER['GOOGLE_CLOUD_REGION']) ){
					self::$gcp_resource['labels']['region'] = $_SERVER['GOOGLE_CLOUD_REGION'];
				}
			} else {
				self::$gcp_resource = false;
			}
		}

		return self::$gcp_resource;
	}
	
	static function getHttpRequest()
	{
		if( null === self::$http_request ){
			self::$http_request = [];
			
			self::$http_request['requestMethod'] = Env::getSchema();
			
			$url = Env::getSchema().'://'.Env::getServerName().$_SERVER['REQUEST_URI'];
			
			self::$http_request['requestUrl'] = $url;
			
			self::$http_request['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
			self::$http_request['remoteIp'] = Env::getIp();
			self::$http_request['serverIp'] = $_SERVER['SERVER_ADDR'];
			
			if( ! empty($_SERVER['HTTP_REFERER']) ){
				self::$http_request['referer'] = $_SERVER['HTTP_REFERER'];
			}
			
			self::$http_request['protocol'] = $_SERVER['SERVER_PROTOCOL'];
			
		}
		
		return self::$http_request;
	}

	/**
	 * 
	 * @param string $key 
	 * @param string|null $value  if null unset label
	 * @return true 
	 */
	static function setGlobalLabel( string $key , string $value = null )
	{
		if( $value ){
			static::$global_labels[$key] = $value;
		} else {
			unset(static::$global_labels[$key]);
		}

		return true;
	}

	/**
	 * 
	 * @return array 
	 */
	static function getGlobalLabels()
	{
		return self::$global_labels;
	}
	
	/**
	 *
	 * @param int $fetch_at_level
	 * @return string[]|null
	 */
	static function getSourceLocation( int $fetch_at_level )
	{
		$backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT|DEBUG_BACKTRACE_IGNORE_ARGS,$fetch_at_level+1);
		
		if( ! isset($backtrace[$fetch_at_level]) ){
			return;
		}
		$backtrace = $backtrace[$fetch_at_level];
		
		$sourceLocation = [];
		
		if( isset($backtrace['class']) ){
			$sourceLocation['function'] = $backtrace['class'] . $backtrace['type'] . $backtrace['function'];
		} else {
			$sourceLocation['function'] = $backtrace['function'];
		}
		
		if( isset($backtrace['file'],$backtrace['line']) ){
			$sourceLocation['file'] = $backtrace['file'];
			$sourceLocation['line'] = $backtrace['line'];
		}
		
		return $sourceLocation;
	}
	
	/**
	 *
	 * @param string $name
	 * @return \Google\Cloud\Logging\Logger
	 */
	static function getLogger( string $name )
	{
		$options = [];
		
		if( self::getGcpResource() ){
			$options['resource'] = self::getGcpResource();
		}
		
		return self::getLogClient()->logger($name , $options);
	}

	/**
	 * Helper function to use with set_error_handler()
	 * @param mixed $severity 
	 * @param mixed $message 
	 * @param mixed $file 
	 * @param mixed $line 
	 * @return \ErrorException 
	 */
	static function errorExceptionGen( $severity, $message, $file, $line )
	{
		return new \ErrorException($message, 0, $severity, $file, $line);
	}

	/**
	 * @param int $severity 
	 * @return string value can be "Notice" , "Warning" , "Parse error" , "Fatal error"
	 */
	static function getGcpErrorType( int $severity )
	{
		if( in_array($severity,[E_NOTICE,E_USER_NOTICE]) ){
			return 'Notice';
		} elseif( in_array($severity,[E_WARNING,E_USER_WARNING,E_CORE_WARNING,E_COMPILE_WARNING]) ){
			return 'Warning';
		} elseif( in_array($severity,[E_PARSE]) ){
			return '"Parse error"';
		} else {
			return 'Fatal error';
		}
	}

	/**
	 *
	 * @param \Throwable $e
	 */
	static function exception( \Throwable $e , $type = null )
	{
		if( $e instanceof \ErrorException && ! $type ){
			$type = self::getGcpErrorType( $e->getSeverity() );
		}

		if( ! $type ){
			$type = 'Fatal error';
		}

		self::writeHelper('reported-error', [
			'@type' => 'type.googleapis.com/google.devtools.clouderrorreporting.v1beta1.ReportedErrorEvent',
			'message' => "PHP $type: $e"
		],[
			'sourceLocation' => self::getSourceLocation(2),
			'httpRequest' => true,
			'labels' => [
				'exception_class' => get_class($e)
			]
		]);
	}
	
	/**
	 *
	 * @param string $logger_name
	 * @param string|array $message
	 * @param array $options
	 */
	static function writeHelper( string $logger_name , $message , $options = [] )
	{
		if( isset($options['httpRequest']) ){
			$options['httpRequest'] = self::getHttpRequest();
		} else {
			unset($options['httpRequest']);
		}
		
		if( isset($options['sourceLocation']) ){
			$options['sourceLocation'] = self::getSourceLocation(2);
		} else {
			unset($options['sourceLocation']);
		}

		if( self::$global_labels ){
			$options['labels'] = ($options['labels']??[]) + self::$global_labels;
		}
		
		try {
			self::getLogger($logger_name)->write($message,$options);
		} catch( \Throwable $e ){
			if( ! is_string($message) ){
				$message = json_encode($message);
			}
			file_put_contents('php://stderr', "GCP Logger write error. $e");
			file_put_contents('php://stderr', $message.PHP_EOL.'Options: '.json_encode($options));
		}
	}
}

