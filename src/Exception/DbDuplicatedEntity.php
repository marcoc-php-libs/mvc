<?php
namespace mrblue\mvc\Exception;

class DbDuplicatedEntity extends MvcException
{
    function __construct( string $entity_name , string $entity_identifier )
    {
        parent::__construct("Entity '$entity_name' duplicated with value '$entity_identifier'");
    }
}