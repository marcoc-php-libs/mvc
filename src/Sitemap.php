<?php

namespace mrblue\mvc;

use mrblue\mvc\Utility\Env;

class Sitemap {
    protected $Mvc;
    protected $Route;

    protected $base_url;

    function __construct(Mvc $Mvc) {
        $this->Mvc = $Mvc;

        $schema = Env::getSchema();

        $this->base_url = $schema . '://' . Env::getServerName();
    }

    /**
     * @return \XMLWriter
     */
    function generate() {

        $sitemap_items = [];
        foreach ($this->Mvc->getRoutes() as $Route) {
            $sitemap_items = array_merge($sitemap_items, $this->routeToArray($Route));
        }

        $XML = new \XMLWriter();
        $XML->openMemory();

        $XML->startDocument('1.0', 'UTF-8');
        $XML->startElement('urlset');

        $XML->startAttribute('xmlns');
        $XML->text('http://www.sitemaps.org/schemas/sitemap/0.9');
        $XML->endAttribute();

        foreach ($sitemap_items as $item) {
            $XML->startElement('url');
            foreach ($item as $tag_name => $tag_value) {
                $XML->startElement($tag_name);
                $XML->text($tag_value);
                $XML->endElement();
            }
            $XML->endElement();
        }

        $XML->endElement();
        $XML->endDocument();

        return $XML;
    }

    protected function routeToArray(Route $Route, string $parent_path = null) {

        $params = $Route->getDefaults(true);

        if (!$Route->isSitemapEnabled(true)) {
            return [];
        }

        $router_controller = $Route->getController(true);
        $router_action = $params['action'] ?? '';
        $ControllerInstance = $this->Mvc->getControllerInstance($router_controller);

        $sitemap_items = [];

        if (!$parent_path) {
            $parent_path = $this->base_url;
        }

        if ($Route->type === \mrblue\mvc\Route::TYPE_LITERAL) {
            $path = $parent_path . $Route->equal_to;
            if ($Route->may_terminate) {
                $item = [
                    'loc' => $path,
                    'changefreq' => $Route->sitemap_changefreq ?? 'monthly'
                ];
                if (method_exists($router_controller, 'sitemapLastmod')) {
                    $lastmod = $ControllerInstance->{'sitemapLastmod'}($router_action);

                    if ($lastmod) {
                        $item['lastmod'] = $lastmod;
                    }
                }
                $sitemap_items[] = $item;
            }
            foreach (clone $Route->getChilds() as $RouteChild) {
                $sitemap_items = array_merge($sitemap_items, $this->routeToArray($RouteChild, $path));
            }
        } elseif ($Route->type === \mrblue\mvc\Route::TYPE_HAYSTACK) {

            $haystack = $Route->getHaystack();

            foreach ($haystack as $haystack_item) {

                $path = $parent_path . preg_replace(
                    '/\:([^\/]+)/',
                    $haystack_item,
                    $Route->equal_to,
                );

                if ($Route->may_terminate) {
                    $item = [
                        'loc' => $path,
                        'changefreq' => $Route->sitemap_changefreq ?? 'monthly'
                    ];
                    $sitemap_items[] = $item;
                }
                foreach (clone $Route->getChilds() as $RouteChild) {
                    $sitemap_items = array_merge($sitemap_items, $this->routeToArray($RouteChild, $path));
                }
            }
        } elseif (method_exists($router_controller, $router_action . 'SitemapGenerator')) {
            $custom_items = $ControllerInstance->{$router_action . 'SitemapGenerator'}($parent_path);
            foreach ($custom_items as &$custom_item) {
                if ($custom_item['loc'][0] == '/') {
                    $custom_item['loc'] = $parent_path . $custom_item['loc'];
                }
            }
            $sitemap_items = array_merge($sitemap_items, $custom_items);

            foreach (clone $Route->getChilds() as $RouteChild) {
                foreach ($custom_items as $custom_item) {
                    $sitemap_items = array_merge($sitemap_items, $this->routeToArray($RouteChild, $custom_item['loc']));
                }
            }
        }

        return $sitemap_items;
    }
}
