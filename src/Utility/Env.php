<?php
namespace mrblue\mvc\Utility;

class Env {

	static public function getIp()
	{
		if( ! empty($_SERVER['HTTP_X_FORWARDED_FOR']) ){
			return trim( explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])[0] );
		} else {
			return $_SERVER['REMOTE_ADDR'] ?? null;
		}
	}
	
	static public function getSchema()
	{
		return ($_SERVER['HTTP_X_FORWARDED_PROTO']??null) ? : ($_SERVER['REQUEST_SCHEME']??null);
	}
	
	static public function getServerName()
	{
		return ($_SERVER['SERVER_NAME']??null) ? : ($_SERVER['HTTP_HOST']??null) ? : $_SERVER['SERVER_ADDR'];
	}
	
	static public function getPort()
	{
		if( ! empty($_SERVER['HTTP_X_FORWARDED_PORT']) ){
			return trim( explode(',',$_SERVER['HTTP_X_FORWARDED_PORT'])[0] );
		} else {
			return $_SERVER['SERVER_PORT'] ?? null;
		}
	}
	

	
}
