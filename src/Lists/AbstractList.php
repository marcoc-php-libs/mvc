<?php
namespace mrblue\mvc\Lists;

abstract class AbstractList implements \Iterator , \Countable , \ArrayAccess
{
	protected $position = 0;
	protected $storage = [];
	
	protected $model_class;
	
	// loading methods
	
	public function __construct( array $items = [] )
	{
		foreach ( $items as $item ){
			$this->add($item);
		}
	}
	
	public function add( $item , $options = [] ) {

		$add['class'] = $this->parse($item);

		if( ! empty($options['attributes']) && is_array($options['attributes']) ){
			$add['attributes'] = $options['attributes'];
		} else {
			$add['attributes'] = [];
		}

		$this->storage[] = $add;
		
		return $this;
	}

	function parse( mixed $item ) :mixed {

		$model_class = $this->model_class;

		if( is_object($item) ){
			
			if( ! is_a($item, $model_class) ){
				throw new \InvalidArgumentException("Item of this list must be instance of $model_class");
			}
		} else {
			$model_class = $model_class::get_final_class($item);
			if( method_exists($model_class,'customConstruct') ){
				$item = $model_class::customConstruct($item);
			} else {
				$item = new $model_class($item);
			}
		}

		return $item;
	}
	
	// reading methods
	
	public function search( string $field , $is , bool $strict = false )
	{
		if( $is instanceof \MongoDB\BSON\ObjectId ){
			$is = (string) $is;
		}

		foreach ( $this->storage as $item ){
			$value = $item['class']->{$field} ?? null;

			if( $value instanceof \MongoDB\BSON\ObjectId ){
				$value = (string) $value;
			}

			if( $strict ){
				if( $value === $is ){
					return $item['class'];
				}
			} else {
				if( $value == $is ){
					return $item['class'];
				}
			}
		}
		
		return null;
	}

	public function filter( string $field , $is , bool $strict = false )
	{
		$FilteredList = new static();

		if( $is instanceof \MongoDB\BSON\ObjectId ){
			$is = (string) $is;
		}

		foreach ( $this->storage as $item ){
			$value = $item['class']->{$field} ?? null;

			if( $value instanceof \MongoDB\BSON\ObjectId ){
				$value = (string) $value;
			}

			if( $strict ){
				if( $value === $is ){
					$FilteredList->add( $item['class'] );
				}
			} else {
				if( $value == $is ){
					$FilteredList->add( $item['class'] );
				}
			}
		}
		
		return $FilteredList;
	}

	public function filterIn( string $field , array $in , bool $strict = false )
	{
		$FilteredList = new static();

		foreach( $in as &$item ){
			if( $item instanceof \MongoDB\BSON\ObjectId ){
				$item = (string) $item;
			}
		}
		unset($item);

		foreach ( $this->storage as $item ){
			$value = $item['class']->{$field} ?? null;

			if( $value instanceof \MongoDB\BSON\ObjectId ){
				$value = (string) $value;
			}

			if( in_array($value,$in,$strict) ){
				$FilteredList->add( $item['class'] );
			}
		}
		
		return $FilteredList;
	}
	
	public function getMax( string $field )
	{
		$max = null;
		
		foreach ( $this->storage as $item ){
			$value = $item['class']->{$field} ?? null;
			if( $max === null || $value > $max ){
				$max = $value;
			}
		}
		
		return $max;
	}
	
	public function getMin( string $field )
	{
		$min = null;
		
		foreach ( $this->storage as $item ){
			$value = $item['class']->{$field} ?? null;
			if( $min === null || $value < $min ){
				$min = $value;
			}
		}
		
		return $min;
	}
	
	/**
	 * 
	 * @param string $field
	 * @param bool $exclude_null
	 * @return mixed[]
	 */
	public function getFieldValues( string $field , bool $exclude_null = true )
	{
		$values = [];
		
		foreach ( $this->storage as $item ){
			$value = $item['class']->{$field} ?? null;
			if( $exclude_null && $value === null ){
				continue;
			}
			$values[] = $value;
		}
		
		return $values;
	}
	
	/**
	 * 
	 * @param string $field_key
	 * @param string $field_value
	 * @param bool $exclude_key_null
	 * @param bool $exclude_value_null
	 * @param bool $create_array_for_duplicated
	 * @return mixed[]
	 */
	public function generateKeyValueMap( string $field_key , string $field_value , bool $exclude_key_null = true , bool $exclude_value_null = false , bool $create_array_for_duplicated = false )
	{
		$map = [];
		
		foreach ( $this->storage as $item ){
			$key = $item['class']->{$field_key} ?? null;
			if( $exclude_key_null && $key === null ){
				continue;
			}
			$value = $item['class']->{$field_value} ?? null;
			if( $exclude_value_null && $value === null ){
				continue;
			}
			if( array_key_exists($key , $map) && $create_array_for_duplicated ){
				$map[$key] = (array) $map[$key];
				$map[$key][] = $value;
			} else {
				$map[$key] = $value;
			}
		}
		
		return $map;
	}
	
	/**
	 * 
	 * @param string|callable $arg1 Model key or callback as php usort() function.
	 * Callback must accept two params. Every param is an array with prop class that contain model
	 * @param int $direction 1 for asc (default), -1 for desc
	 * @param bool $null_before if true null > any other value, default false
	 * @param bool $use_attributes if true sort happen based on attributes instead class properties, default false
	 * @return \Intoway\Core\Lists\AbstractList
	 */
	function sort( $arg1 , $direction = 1 , $null_greater = false , $use_attributes = false ) {
		if( is_string($arg1)) {
			usort( $this->storage , function($a , $b) use ($arg1,$direction,$null_greater,$use_attributes){
				if( $use_attributes ){
					$el1 = $a['attributes'][$arg1] ?? null;
					$el2 = $b['attributes'][$arg1] ?? null;
				} else {
					$el1 = $a['class']->{$arg1} ?? null;
					$el2 = $b['class']->{$arg1} ?? null;
				}
				
				$Abest = $el1 >= $el2 ? 1 : -1;
				if( $el1 === null && $null_greater ){
					$Abest = 1;
				} elseif( $el2 === null && $null_greater ){
					$Abest = -1;
				}
				
				return $Abest * ( $direction > 0 ? 1 : -1 );
			});
		} elseif( is_callable($arg1) ){
			usort( $this->storage , function($a , $b) use($arg1){
				return $arg1( $a , $b );
			});
		} else {
			throw new \RuntimeException('Argument 1 must be string or callable');
		}
		
		return $this;
	}

	/**
	 * Export all childs as array of object
	 * @return array 
	 */
	public function export( array $model_options = [] )
	{
		$export = [];
		foreach( $this->storage as $item ){
			$export[] = $item['class']->export( $model_options );
		}

		return $export;
	}
	
	// Iterator methods
	
	public function rewind() : void
	{
		$this->position = 0;
	}
	
	public function current() : mixed
	{
		return $this->storage[$this->position]['class'];
	}
	
	public function key() : mixed
	{
		return $this->position;
	}
	
	public function next() : void
	{
		++$this->position;
	}
	
	public function valid() : bool
	{
		return isset($this->storage[$this->position]);
	}

	// Countable methods
	
	public function count() : int
	{
		return count($this->storage);
	}

	// ArrayAccess methods

	public function offsetExists(mixed $offset): bool {
		return array_key_exists($offset,$this->storage);
	}
	public function offsetGet(mixed $offset): mixed {
		return $this->storage[$offset]['class'] ?? null;
	}
	public function offsetSet(mixed $offset, mixed $value): void {
		$this->storage[$offset]['class'] = $this->parse($value);
	}
	public function offsetUnset(mixed $offset): void {
		unset($this->storage[$offset]);
	}

	public function hasAttribute( $key )
	{
		return array_key_exists($key,$this->storage[$this->position]['attributes']);
	}

	public function getAttribute( $key )
	{
		return $this->storage[$this->position]['attributes'][$key] ?? null;
	}

	public function setAttribute( $key , $value )
	{
		$this->storage[$this->position]['attributes'][$key] = $value;
		return $this;
	}

	public function setAttributes( $values )
	{
		$this->storage[$this->position]['attributes'] = $values + $this->storage[$this->position]['attributes'];
		return $this;
	}
}

