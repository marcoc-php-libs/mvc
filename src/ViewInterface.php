<?php
namespace mrblue\mvc;

interface ViewInterface
{
    /**
     * @return mixed Be careful, if data is an array and not an object, var will not be referenced
     * and update on returned var will not be reflected in View data property
     */
    public function getData();

    /**
     * @param mixed $data must be an array, an instance of \ArrayAccess or an instance of \stdClass
     */
    public function setData( $data );

    public function render();
}

