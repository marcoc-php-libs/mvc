<?php
namespace mrblue\mvc;

abstract class AbstractController {

    protected $Mvc;

    public function setMvc( Mvc $Mvc )
    {
        $this->Mvc = $Mvc;
        return $this;
    }

}