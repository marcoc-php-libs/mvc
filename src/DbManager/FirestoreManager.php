<?php
namespace mrblue\mvc\DbManager;

use Google\Cloud\Firestore\CollectionReference;
use Google\Cloud\Firestore\DocumentReference;
use Google\Cloud\Firestore\DocumentSnapshot;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Firestore\Query;
use mrblue\mvc\Lists\AbstractList;
use mrblue\mvc\Model\AbstractModel;

abstract class FirestoreManager {
	
	protected FirestoreClient $Connection;
	protected CollectionReference $Collection;

	protected $collection_name;
	
	protected $model_class;
	protected $list_class;

	protected $model_id_field = 'id';

	protected ?array $default_order_by = null;
	protected ?int $default_limit = null;
	
	function __construct( FirestoreClient $Connection ) {
		$this->Connection = $Connection;
		$this->Collection = $Connection->collection($this->collection_name);
	}
	
	/**
	 * Standard get() method. Override it if need
	 */
	function get( string $name ) : ?AbstractModel {

		$DocumentSnapshot = $this->internalGet($name);

		if( ! $DocumentSnapshot->exists() ){
			return null;
		}
		
		return $this->createModel($DocumentSnapshot);
	}
	
	/**
	 * Standard search() method. Override it if need
	 */
	function search( $query = null ) : AbstractList{

		if( $query === null || is_array($query) ){
			$Query = $this->Collection;

			$order_by = $query['order_by'] ?? $this->default_order_by;

			if( $order_by ){
				foreach( $order_by as $field => $mode ){
					$Query = $Query->orderBy( $field , $mode );
				}
			}

			$limit = $query['limit'] ?? $this->default_limit;

			if( $limit ){
				$Query = $Query->limit($limit);
			}

			$offset = $query['offset'] ?? null;

			if( $offset ){
				$Query = $Query->offset($offset);
			}

			if( isset($query['where']) ){
				foreach($query['where'] as $field => $value ){
					$Query = $Query->where($field , '=' , $value);
				}
			}
		} elseif( $query instanceof Query || $query instanceof CollectionReference ){
			$Query = $query;
		} else {
			throw new \InvalidArgumentException("Query must be instance of ".Query::class." or ".CollectionReference::class);
		}
		
		$list_class = $this->list_class;
		$List = new $list_class();
		foreach( $Query->documents() as $DocumentSnapshot ){
			$List->add( $this->createModel($DocumentSnapshot) );
		}

		return $List;
	}

	protected function insertDocument( array $data ) : DocumentReference {
		if( array_key_exists($this->model_id_field , $data) ){
			$Document = $this->Collection->document($data[$this->model_id_field]);
			unset($data[$this->model_id_field]);
		} else {
			$Document = $this->Collection->newDocument();
		}

		$Document->create($data);
		return $Document;
	}

	function insert( array $data , array $options = [] ) : DocumentReference {

		$model_class = $this->model_class::get_final_class($data);

		return $this->insertDocument($model_class::getDbInsertQuery($data) , $options);
	}
	

	protected function updateDocument( string $name , array $data , array $options = [] ) : DocumentReference {
		$Document = $this->Collection->document($name);
		$Document->update($data , $options);
		return $Document;
	}

	protected function setDocument( string $name , array $data , array $options = [] ) : DocumentReference {
		$Document = $this->Collection->document($name);
		$Document->set($data , $options);
		return $Document;
	}

	function update( string $name , array $data , array $options = [] ) : ?DocumentReference {
		$model_class = $this->model_class::get_final_class($data);
		$query = $model_class::getDbUpdateQuery($data);

		if( ! empty($query['$set']) ){
			return $this->setDocument($name , $query['$set'] , [
				'merge' => true
			]);
		} elseif( ! empty($query['$update']) ){
			return $this->updateDocument($name , $query['$update']);
		} else {
			return null;
		}

	}
	
	protected function deleteDocument( string $name ) {
		return $this->Collection->document($name)->delete();
	}

	function delete( string $name ) {
		return $this->deleteDocument($name);
	}
	
	protected function internalGet( string $name ) : DocumentSnapshot {
		return $this->Collection->document($name)->snapshot();
	}

	function getCollection() : CollectionReference {
		return $this->Collection;
	}

	function createModel( DocumentSnapshot $DocumentSnapshot ) : AbstractModel {
		$data = $DocumentSnapshot->data();
		$model_class = $this->model_class::get_final_class($data);
		$data[$this->model_id_field] = $DocumentSnapshot->id();
		return new $model_class( $model_class::db2model($data) );
	}
}