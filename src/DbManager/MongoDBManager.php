<?php
namespace mrblue\mvc\DbManager;

use mrblue\mvc\Exception\DbDuplicatedEntity;
use mrblue\mvc\Helper\DbExcludedField;

abstract class MongoDBManager
{
	CONST TYPE_MAP = [
		'array' => 'array',
		'document' => 'array',
		'root' => 'array',
	];

	CONST REQUIRED_READ_OPTIONS = [
		'typeMap' => self::TYPE_MAP
	];

	CONST DEFAULT_CHILDS_PKEY = 'id';
	
	/**
	 * @var \MongoDB\Database
	 */
	protected $Connection;
	/**
	 * @var \MongoDB\Collection
	 */
	protected $Collection;
	protected $collection_name;
	protected $model_pkey = '_id';
	protected $childs_pkey_map = [];
	protected $enable_child_pkey_autoincrement = true;
	
	protected $model_class;
	protected $list_class;
	
	protected $default_sort = null;
	protected $default_limit = null;
	protected $default_skip = null;

	protected $child_models = [];
	
	public function __construct( \MongoDB\Database $Connection )
	{
		$this->Connection = $Connection;
		$this->Collection = $Connection->selectCollection($this->collection_name);
	}
	
	/**
	 * Standard get() method. Override it if need
	 * @param mixed $id
	 * @return \mrblue\mvc\Model\AbstractModel|null
	 */
	public function get( $id , array $options = [] )
	{
		return $this->internalGet(['_id' => $this->filterId($id)] , $options );	
	}
	
	/**
	 * Standard search() method. Override it if need
	 * @param array $query
	 * @param array $options
	 * @return \mrblue\mvc\Lists\AbstractList
	 */
	public function search( array $query , array $options = [] )
	{
		return $this->internalSearch($query , $options);
	}

	/**
	 * 
	 * @param mixed $data
	 * @param array $options
	 * @return \MongoDB\InsertOneResult
	 */
	protected function insertDocument( $data , array $options = [] )
	{
		return $this->Collection->insertOne( $data , $options );
	}

	/**
	 *
	 * @param array $query_parent
	 * @param string $child_name
	 * @param array $child_data
	 * @return mixed child_id
	 */
	protected function insertDocumentChild( array $query_parent , string $child_name , array $child_data )
	{
		$pkey = $this->getChildPKey($child_name);

		if( isset($child_data[ $pkey ]) ){
			$child_id = $child_data[ $pkey ];
			if( is_int( $child_id ) ){
				$this->setSeq($query_parent,$child_name,$child_id,true);
			}
		} elseif( $this->enable_child_pkey_autoincrement ) {
			$child_id = $this->generateChildId($query_parent, $child_name);
			$child_data = [$pkey => $child_id] + $child_data;
		} else {
			throw new \RuntimeException("PKey field '$pkey' not passed, and autoincrement is disabled");
		}

		$modified = $this->updateDocument($query_parent + [
			'$nor' => [
				[
					$child_name => [
						'$elemMatch' => [
							$pkey => $child_id
						]
					]
				]
			]
		], [
			'$push' => [
				$child_name => $child_data
			]
		])->getModifiedCount();

		if( ! $modified ){
			$parent_id = $query_parent[ $this->model_pkey ] ?? null;
			if( $parent_id ){
				$entity_identifier = $parent_id . '.' . $child_id;
			} else {
				$entity_identifier = $child_id;
			}
			throw new DbDuplicatedEntity($this->collection_name.'.'.$child_name , $entity_identifier );
		}
		
		return $child_id;
	}

	/**
	 * Standard insert() method. Override it if need
	 * @param array $data
	 * @return mixed inserted id
	 */
	public function insert( array $data , array $options = [] )
	{
		$model_class = $this->model_class::get_final_class($data);

		return $this->insertDocument(
			$model_class::getDbInsertQuery($data) ,
			$options['mongodb_options'] ?? []
		)->getInsertedId();
	}

	/**
	 * Standard insertChild() method. Override it if need
	 * @param mixed $id passed at get() method
	 * @param string $child_name
	 * @param array $child_data
	 * @return NULL|int $child_id
	 */
	public function insertChild( $id , string $child_name , array $child_data )
	{
		$child_model = $this->child_models[$child_name] ?? null;
		if( ! $child_model ){
			throw new \DomainException("Child '$child_name' not valid");
		}
		$child_model = $child_model::get_final_class($child_data);
		
		$Target = $this->get($id);
		
		if( ! $Target ){
			return null;
		}
		
		return $this->insertDocumentChild([
			'_id' => $Target->{$this->model_pkey}
		], $child_name, $child_model::getDbInsertQuery($child_data));
	}
	
	/**
	 * 
	 * @param array $query_parent
	 * @param array $update
	 * @param array $options
	 * @return \MongoDB\UpdateResult
	 */
	protected function updateDocument( array $query_parent , array $update , array $options = [] )
	{
		return $this->Collection->updateOne( $query_parent , $update , $options );
	}

	/**
	 *
	 * @param array $query_parent
	 * @param string $child
	 * @param $child_id
	 * @param array $update
	 * @return \MongoDB\UpdateResult
	 */
	protected function updateDocumentChild( array $query_parent , string $child_name , $child_id , array $child_update )
	{
		$pkey = $this->getChildPKey($child_name);

		$query_parent["$child_name.$pkey"] = $child_id;
		
		$update = [];
		foreach ($child_update as $operator => $operator_update){
			foreach ($operator_update as $key => $value){
				$update[$operator][$child_name.'.$.'.$key] = $value;
			}
		}

		if( ! $update ){
			return null;
		}

		return $this->updateDocument($query_parent, $update);
	}

	/**
	 * Standard update() method. Override it if need
	 * @param mixed $id
	 * @param array $update
	 * @return int contain the num of matched rows. Should be 1
	 */
	public function update( $id , array $update , array $options = [] )
	{
		$model_class = $this->model_class::get_final_class($update);
		$update_query = $model_class::getDbUpdateQuery($update);

		if( ! $update_query ){
			return null;
		}

		return $this->updateDocument(
			['_id'=> $this->filterId($id)] ,
			$update_query ,
			$options['mongodb_options'] ?? []
		)->getMatchedCount();
	}

	/**
	 * Standard updateChild() method. Override it if need
	 * @param $id passed at get() method
	 * @param string $child
	 * @param $child_id
	 * @param array $update
	 * @return NULL|int $modified
	 */
	public function updateChild( $id , string $child_name , $child_id , array $child_update )
	{
		$child_model = $this->child_models[$child_name] ?? null;
		if( ! $child_model ){
			throw new \DomainException("Child '$child_name' not valid");
		}
		$child_model = $child_model::get_final_class($child_update);
		
		$Target = $this->get($id);
		
		if( ! $Target ){
			return null;
		}
		
		$update_query = $child_model::getDbUpdateQuery($child_update);

		if( ! $update_query ){
			return null;
		}

		return $this->updateDocumentChild([
			'_id' => $Target->{$this->model_pkey}
		], $child_name, $child_id, $update_query)->getMatchedCount();
	}
	
	/**
	 * 
	 * @param array $query_parent
	 * @param array $options
	 * @return \MongoDB\DeleteResult
	 */
	protected function deleteDocument( array $query_parent , array $options = [] )
	{
		return $this->Collection->deleteOne( $query_parent , $options );
	}
	
	/**
	 *
	 * @param array $query_parent
	 * @param string $child
	 * @param $child_id
	 * @return \MongoDB\UpdateResult
	 */
	protected function deleteDocumentChild( array $query_parent , string $child_name , $child_id )
	{
		$pkey = $this->getChildPKey($child_name);

		return $this->updateDocument($query_parent, [
			'$pull' => [
				$child_name => [
					$pkey => $child_id
				]
			]
		]);
	}

	/**
	 * Standard delete() method. Override it if need
	 * @param $id
	 * @return int contain the num of deleted rows. Should be 1
	 */
	public function delete( $id , array $options = [] )
	{
		return $this->deleteDocument(['_id' => $this->filterId($id)], $options['mongodb_options'] ?? [])->getDeletedCount();
	}

	/**
	 * Standard deleteChild() method. Override it if need
	 * @param $id passed at get() method
	 * @param string $child
	 * @param $child_id
	 * @param array $update
	 * @return NULL|\MongoDB\UpdateResult
	 */
	public function deleteChild( $id , string $child_name  , $child_id )
	{
		$Target = $this->get($id);
		
		if( ! $Target ){
			return null;
		}
		
		return $this->deleteDocumentChild([
			'_id' => $Target->{$this->model_pkey}
		], $child_name, $child_id)->getModifiedCount();
	}

	/**
	 * Standard sortChild() method. Override it if need
	 * @param $id passed at get() method
	 * @param string $child_name
	 * @param $child_id
	 * @param array $update
	 * @return NULL|int $modified
	 */
	public function sortChild( $id , string $child_name , $child_id , int $new_position ) {

		$Target = $this->get($id);
		
		if( ! $Target ){
			return null;
		}

		return $this->sortDocumentChild([
			'_id' => $Target->{$this->model_pkey}
		], $child_name, $child_id, $new_position)->getMatchedCount();
	}

	/**
	 *
	 * @return \MongoDB\UpdateResult
	 */
	protected function sortDocumentChild( array $query_parent , string $child_name , $child_id , int $new_position ) {

		if ( $new_position < 0 ){
			throw new \InvalidArgumentException("new_position must be >= 0");
		}

		$pkey = $this->getChildPKey($child_name);

		$query = $query_parent;
		$query["$child_name.$pkey"] = $child_id;
		$tmp_field = "_TMP_$child_name";
		$tmp_field_size = "_SIZE_$tmp_field";
		
		return $this->Collection->updateOne($query,[
			[
				'$set' => [
					$tmp_field => [
						'$filter' => [
							'input' => '$'.$child_name,
							'cond' => [
								'$ne' => ['$$this.'.$pkey , $child_id]
							]
						]
					]
				]
			],
			[
				'$set' => [
					$tmp_field_size => [
						'$size' => '$'.$child_name,
					]
				]
			],
			[
				'$set' => [
					$tmp_field => [
						'$concatArrays' => [
							$new_position > 0 ? [
								'$slice' => ['$'.$tmp_field,0,$new_position]
							] : [],
							[
								'$filter' => [
									'input' => '$'.$child_name,
									'cond' => [
										'$eq' => ['$$this.'.$pkey , $child_id]
									]
								]
							],
							$new_position > 0 ? [
								'$slice' => ['$'.$tmp_field,$new_position,'$'.$tmp_field_size]
							] : '$'.$tmp_field,
						]
					]
				]
			],
			[
				'$set' => [
					$child_name => '$'.$tmp_field
				]
			],
			[
				'$unset' => [ $tmp_field, $tmp_field_size]
			]	
		]);
	}
	
	protected function internalGet( array $query , array $options = [] )
	{
		$pipe[]['$match'] = $query;

		if( ! empty($options['childs_filters']) ){
			$pipe[]['$project'] = $this->getChildsFiltersProject($options['childs_filters']);
		}

		if( ! empty($options['childs_limits']) ){
			$pipe[]['$project'] = $this->getChildsLimitsProject($options['childs_limits']);
		}

		$document = $this->Collection->aggregate($pipe , self::REQUIRED_READ_OPTIONS)->toArray()[0] ?? null;
		
		if( ! $document ){
			return null;
		}
		
		if( ! empty($options['childs_filters']) ){
			$document = $this->parseFilteredDocument($document , $options['childs_filters']);
		}

		$model_class = $this->model_class::get_final_class($document);
		
		return new $model_class( $model_class::db2model($document) );
	}

	protected function internalSearch( array $query , array $options = [] )
	{
		$model_class = $this->model_class;
		$list_class = $this->list_class;

		$pipe = [];
		if( $query ){
			$pipe[]['$match'] = $query;
		}
		
		$sort = $options['sort'] ?? $this->default_sort;
		$limit = $options['limit'] ?? $this->default_limit;
		$skip = $options['skip'] ?? $this->default_skip;
		
		if( $sort ){
			$pipe[]['$sort'] = $sort;
		}
		if( $skip ){
			$pipe[]['$skip'] = $skip;
		}
		if( $limit ){
			$pipe[]['$limit'] = $limit;
		}

		if( ! empty($options['childs_filters']) ){
			$pipe[]['$project'] = $this->getChildsFiltersProject($options['childs_filters']);
		}

		if( ! empty($options['childs_limits']) ){
			$pipe[]['$project'] = $this->getChildsLimitsProject($options['childs_limits']);
		}
		
		$Cursor = $this->Collection->aggregate($pipe , self::REQUIRED_READ_OPTIONS);
		
		$List = new $list_class();
		
		foreach( $Cursor as $Document ){
			if( ! empty($options['childs_filters']) ){
				$Document = $this->parseFilteredDocument($Document , $options['childs_filters']);
			}
			$model_class = $model_class::get_final_class($Document);
			$List->add( $model_class::db2model($Document) );
		}

		return $List;
	}

	/**
	 * 
	 * @param array $query_parent
	 * @param string $child
	 * @throws \RuntimeException
	 * @return int
	 */
	protected function generateChildId( array $query_parent , string $child )
	{
		return $this->internalIncSeq( $query_parent , $child , 1 );
	}

	/**
	 * 
	 * @param array $query_parent
	 * @param string $name
	 * @param int $amount default 1
	 * @throws \RuntimeException
	 * @return int
	 */
	protected function internalIncSeq( array $query_parent , string $name , int $amount = 1 )
	{
		$Document = $this->Collection->findOneAndUpdate($query_parent,[
			'$inc' => [
				"seq.$name" => $amount
			]
		],self::REQUIRED_READ_OPTIONS + [
			'projection' => [
				"seq.$name" => 1
			],
			'returnDocument' => \MongoDB\Operation\FindOneAndUpdate::RETURN_DOCUMENT_AFTER
		]);
		
		if( ! $Document ){
			throw new \RuntimeException("Document not exists");
		}
		
		return $Document['seq'][$name];
	}

	/**
	 * Standard incSeq() method. Override it if need
	 * @param $id
	 * @return int contain generated id
	 */
	public function incSeq( $id , string $name , int $amount = 1  )
	{
		return $this->internalIncSeq(['_id' => $this->filterId($id)], $name , $amount );
	}


	/**
	 * 
	 * @param array $query_parent
	 * @param string $name
	 * @param int $value default
	 * @param bool $set_only_if_gt=false
	 * @return int 0 or 1, the number of edited documens
	 */
	protected function internalSetSeq( array $query_parent , string $name , int $value , bool $set_only_if_gt = false )
	{
		$update_operator = $set_only_if_gt ? '$max' : '$set';

		$Document = $this->Collection->findOneAndUpdate($query_parent,[
			$update_operator => [
				"seq.$name" => $value
			]
		],self::REQUIRED_READ_OPTIONS + [
			'projection' => [
				"seq.$name" => 1
			],
			'returnDocument' => \MongoDB\Operation\FindOneAndUpdate::RETURN_DOCUMENT_AFTER
		]);
		
		return $Document ? 1 : 0;
	}

	/**
	 * Standard setSeq() method. Override it if need
	 * @param $id
	 * @param string $name
	 * @param int $value
	 * @param bool $set_only_if_gt
	 * @return int contain generated id
	 */
	public function setSeq( $id , string $name , int $value , bool $set_only_if_gt = false  )
	{
		return $this->internalSetSeq(['_id' => $this->filterId($id)], $name , $value , $set_only_if_gt );
	}

	public function getChildPKey( string $child_name )
	{
		return $this->childs_pkey_map[$child_name] ?? self::DEFAULT_CHILDS_PKEY;
	}

	protected function getChildsFiltersProject( array $filters )
	{
		if( empty($filters) || ! is_array($filters) ){
			throw new \InvalidArgumentException('"filters" field must be a not empty array');
		}

		$project = [];
		foreach( $filters as $key => $filter ){
			if( $filter === null ){
				$project[$key] = false;
			} elseif( $filter instanceof DbExcludedField ) {
				$project[$key] = false;
			} elseif( is_array($filter) ) {
				$project[$key]['$filter'] = [
					'input' => '$'.$key,
					'as' => 'item',
					'cond' => $filter
				];
			} else {
				throw new \InvalidArgumentException('"filter" value must be an array or a null value');
			}
		}

		return $project;
	}

	protected function getChildsLimitsProject( array $limits )
	{
		if( empty($limits) || ! is_array($limits) ){
			throw new \InvalidArgumentException('"limits" field must be a not empty array');
		}

		$project = [];
		foreach( $limits as $key => $limit ){
			if( ! isset($limit['limit']) ){
				throw new \InvalidArgumentException('"limit" field is required');
			}

			if( isset($limit['skip']) ){
				$project[$key]['$slice'] = ['$'.$key , $limit['skip'] , $limit['limit']];
			} else {
				$project[$key]['$slice'] = ['$'.$key , $limit['limit']];
			}
		}

		return $project;
	}

	protected function parseFilteredDocument( $document , array $filters )
	{
		foreach( $filters as $key => $filter ){
			if( $filter instanceof DbExcludedField ) {
				$document[$key] = new DbExcludedField;
			}
		}

		return $document;
	}

	/**
	 * Extend if need. This function filter id before queries
	 */
	public function filterId( $id )
	{
		return $id;
	}
}

